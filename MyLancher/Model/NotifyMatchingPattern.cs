﻿/* NotifyMatchingPattern.cs */
using System; /* String */
using System.ComponentModel; /* INotifyPropertyChanged */

namespace MyLancher.Model
{
    /// <summary>
    /// カタログの設定タブの検索パターン一覧表示のためのクラス
    /// </summary>
    public class NotifyMatchingPattern : INotifyPropertyChanged
    {
        private string _pattern;

        public string Pattern
        {
            get
            {
                return _pattern;
            }
            set
            {
                if (_pattern != value)
                {
                    _pattern = value;
                    NotifyPropertyChanged("Pattern");
                }
            }
        }

        public NotifyMatchingPattern(string pattern)
        {
            Pattern = pattern;
        }

        public override string ToString()
        {
            return Pattern;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
