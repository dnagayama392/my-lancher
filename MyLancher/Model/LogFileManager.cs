﻿/* LogFileManager */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO; /* File, Path */
using System.Diagnostics; /* Process */

namespace MyLancher.Model
{
    public class LogFileManager
    {
        private string logPath;

        public string LogPath
        {
            get
            {
                return logPath;
            }
            set
            {
                if (logPath != value)
                {
                    logPath = value;
                }
            }
        }

        public LogFileManager()
        {
            var curDir = Directory.GetCurrentDirectory();
            LogPath = curDir + @"\log.txt";
            if(!File.Exists(LogPath))
            {
                Create();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="exceptionMessage"></param>
        public void Add(String exceptionMessage)
        {
            File.AppendAllText(LogPath, exceptionMessage + "\r\n", Encoding.UTF8);
        }

        public void Add(String exceptionClass, String exceptionMessage)
        {
            File.AppendAllText(LogPath, exceptionClass + ":" + exceptionMessage + "\r\n", Encoding.UTF8);
        }

        /// <summary>
        /// ログファイルを実行します（既定のテキストエディタで実行されます）。
        /// </summary>
        public void Execute()
        {
            Process.Start(LogPath);
        }

        /// <summary>
        /// ログファイルを作成します。
        /// </summary>
        private void Create()
        {
            var str = "";
            File.WriteAllText(LogPath, str);
        }

    }
}
