﻿/* IndexListManager.cs */
using System.Text.RegularExpressions; /* Regex */

namespace MyLancher.Model
{
    public class IndexListManager
    {
        private IndexCatalog indexItemList;
        private SearchedItemList itemList;
        private string previousString;
        private StringList previousNameList;
        // 検索オプション //
        private bool notIgnoreCase;
        private bool wildAvailability;

        #region Properties
        public IndexCatalog IndexItemList
        {
            get
            {
                return indexItemList;
            }
            set
            {
                if (indexItemList != value)
                {
                    indexItemList = value;
                }
            }
        }

        public SearchedItemList ItemList
        {
            get
            {
                return itemList;
            }
            set
            {
                if (itemList != value)
                {
                    itemList = value;
                }
            }
        }

        public bool NotIgnoreCase
        {
            get
            {
                return notIgnoreCase;
            }
            set
            {
                if (notIgnoreCase != value)
                {
                    notIgnoreCase = value;
                }
            }
        }

        public bool WildAvailability
        {
            get
            {
                return wildAvailability;
            }
            set
            {
                if (wildAvailability != value)
                {
                    wildAvailability = value;
                }
            }
        }

        public string PreviousString
        {
            get
            {
                return previousString;
            }
            set
            {
                if (previousString != value)
                {
                    previousString = value;
                }
            }
        }

        public StringList PreviousNameList
        {
            get
            {
                return previousNameList;
            }
            set
            {
                if (previousNameList != value)
                {
                    previousNameList = value;
                }
            }
        }
        #endregion

        public IndexListManager()
        {
            Initialize();
        }

        public void SetSettingStatus()
        {
            var settingFile = new GeneralSettingFileManager();
            var settingData = settingFile.GetSettingData();
            SetSettingStatus(settingData);
        }

        public void SetSettingStatus(LauncherSettingData settingData)
        {
            NotIgnoreCase = settingData.NotIgnoreCase;
            WildAvailability = settingData.WildAvailability;
        }

        public SearchedItemList GetTargetItemList(string str)
        {
            ItemList = new SearchedItemList();
            if (str == "")
            {
                return itemList;
            }
            // HACK: 検索方法の検討
            ItemList.Marge(GetItemListOfPartialMatch(str));
            PreviousString = str;
            return ItemList;
        }

        private void Initialize()
        {
            IndexItemList = new IndexCatalog();
            ItemList = new SearchedItemList();
            PreviousString = "";
            PreviousNameList = new StringList();
            IndexItemList.ReadIndexData();
            SetSettingStatus();
        }

        /// <summary>
        /// 名前が部分一致しているアイテムのリストを返す。
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private SearchedItemList GetItemListOfPartialMatch(string str)
        {
            Regex partialMatch = new Regex(@"\w*" + GetCheckString(str) + @"\w*");
            var matchItemList = GetMatchedItemList(str, partialMatch);
            var defaultItemList = GetMatchedDefaultItems(partialMatch);
            matchItemList.Marge(defaultItemList);
            return matchItemList;
        }

        private SearchedItemList GetMatchedDefaultItems(Regex matchPattern)
        {
            var list = new SearchedItemList();
            list.SetDefaultMenu();
            var defaultList = list.GetItemList();
            list.Clear();
            foreach (var defaultItem in defaultList)
            {
                if (matchPattern.IsMatch(defaultItem.ItemName))
                {
                    list.Add(defaultItem);
                }
            }
            return list;
        }

        private string GetCheckString(string str)
        {
            string newStr = @"";
            if (!NotIgnoreCase)
            {
                newStr += @"(?i)";
            }
            if (WildAvailability)
            {
                str.Replace(@"*", @"\w*");
                str.Replace(@"?", @"\w?");
            }
            newStr += str;
            return newStr;
        }

        private SearchedItemList GetMatchedItemList(string str, Regex matchPattern)
        {
            var strNameList = GetNameList(str);
            var nameList = strNameList.GetList();
            SearchedItemList list = new SearchedItemList();
            foreach (var itemName in nameList)
            {
                if (matchPattern.IsMatch(itemName))
                {
                    var itemData = IndexItemList.GetItemDataNameOf(itemName);
                    list.Add(itemData);
                }
            }
            PreviousNameList = strNameList;
            return list;
        }

        private StringList GetNameList(string str)
        {
            if (PreviousString.Length == 0)
            {
                return IndexItemList.GetNameList();
            }
            if (str.Length < PreviousString.Length)
            {
                return IndexItemList.GetNameList();
            }
            return PreviousNameList;
        }
    }
}
