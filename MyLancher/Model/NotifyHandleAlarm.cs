﻿/* NotifyHandleAlarm.cs */
using System; /* String */
using System.ComponentModel; /* INotifyPropertyChanged, PropertyChangedEventHandler */

namespace MyLancher.Model
{
    public class NotifyHandleAlarm : INotifyPropertyChanged
    {


        // Their methods defined by INotifyPropertyChanged. 
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
