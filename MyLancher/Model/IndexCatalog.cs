﻿/* TargetCatalog.cs */

namespace MyLancher.Model
{
    /// <summary>
    /// インデックスアイテムを格納するクラス
    /// </summary>
    public class IndexCatalog
    {
        #region Member Variables
        private IndexFileManager indexManager;
        private SearchedItemList itemsList;
        private int count;
        #endregion

        #region Properties
        private IndexFileManager IndexManager
        {
            get
            {
                return indexManager;
            }
            set
            {
                if(indexManager != value)
                {
                    indexManager = value;
                }
            }
        }

        private SearchedItemList ItemList
        {
            get
            {
                return itemsList;
            }
            set
            {
                if(itemsList != value)
                {
                    itemsList = value;
                }
            }
        }

        public int Count
        {
            get
            {
                return count;
            }
            set
            {
                if (count != value)
                {
                    count = value;
                }
            }
        }
        #endregion

        // コンストラクタ //
        public IndexCatalog()
        {
            Initialize();
        }

        // public メソッド //
        public void Clear()
        {
            Initialize();
            ReadIndexData();
        }

        public SearchedItem GetItemDataNameOf(string name)
        {
            return ItemList.GetItemDataNameOf(name);
        }

        public StringList GetNameList()
        {
            return ItemList.GetItemNameList();
        }

        public void ReadIndexData()
        {
            ItemList = IndexManager.ReadIndexList();
            Count = ItemList.Count;
        }

        public void RemakeIndexFile()
        {
            IndexManager.Remake();
        }

        // private メソッド //
        private void Initialize()
        {
            IndexManager = new IndexFileManager();
            ItemList = new SearchedItemList();
            //ReadIndexData();
        }
    }
}
