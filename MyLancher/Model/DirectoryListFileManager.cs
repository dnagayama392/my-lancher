﻿using System; /* Environment */
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel; /* Collection */
using System.IO; /* File */

namespace MyLancher.Model
{
    /// <summary>
    /// DirectoryList.csvの入出力を行なうクラス
    /// </summary>
    public class DirectoryListFileManager
    {
        private const string DIRECTORY_LIST_FILE = @"directry_list.csv";
        public const string DIRECTORY_LIST_FILE_PATH
            = DataDirectoryManager.DATA_DIRECTORY_PATH + DataDirectoryManager.PATH_SEPARATE
            + DIRECTORY_LIST_FILE;

        private Collection<DirectoryData> directoryList;

        private Collection<DirectoryData> DirectoryList
        {
            get
            {
                return directoryList;
            }
            set
            {
                directoryList = value;
            }
        }

        public DirectoryListFileManager()
        {
            Initialize();
        }

        public DirectoryDataList GetDirectoryDataList()
        {
            return new DirectoryDataList(DirectoryList);
        }

        public void Write(DirectoryDataList newDirectoryList)
        {
            var newDirectoryCollection = newDirectoryList.GetCollectionList();
            //if (DirectoryList == newDirectoryCollection)
            //{
            //    return;
            //}
            DirectoryList = newDirectoryCollection;
            WriteDirectoryListFile();
        }

        private void WriteDirectoryListFile()
        {
            var str = "";
            foreach (var directoryData in DirectoryList)
            {
                str += directoryData.DirectoryPath;
                foreach (var pattern in directoryData.DirectoryMatchFormat)
                {
                    str += "," + pattern;
                }
                str += "\r\n";
            }
            File.WriteAllText(DIRECTORY_LIST_FILE_PATH, str);
        }

        private void Initialize()
        {
            if (!Directory.Exists(DataDirectoryManager.DATA_DIRECTORY_PATH))
            {
                Directory.CreateDirectory(DataDirectoryManager.DATA_DIRECTORY_PATH);
            }
            CreateDirectoryListFile();
            DirectoryList = new Collection<DirectoryData>();
            SetDirectoryList();
        }

        /// <summary>
        /// DirectryList.csvが存在しなければ作成（初期値あり）。
        /// </summary>
        private void CreateDirectoryListFile()
        {
            if (File.Exists(DIRECTORY_LIST_FILE_PATH))
            {
                return;
            }
            var defaultString = "";
            // DONE: 環境によってスタートメニューのパスが設定されるように変更
            defaultString += Environment.GetFolderPath(Environment.SpecialFolder.CommonStartMenu) + ",*.lnk" + "\r\n";
            defaultString += Environment.GetFolderPath(Environment.SpecialFolder.StartMenu) + ",*.lnk" + "\r\n";
            CreateDirectoryListFile(defaultString);
        }

        /// <summary>
        /// DirectryList.csvが存在しなければ作成。
        /// </summary>
        /// <param name="defaultString">初期文字列</param>
        private void CreateDirectoryListFile(string defaultString)
        {
            if (!File.Exists(DIRECTORY_LIST_FILE_PATH))
            {
                File.AppendAllText(DIRECTORY_LIST_FILE_PATH, defaultString);
            }
        }

        /// <summary>
        /// DirectoryList に値を設定
        /// </summary>
        private void SetDirectoryList()
        {
            var allLinesArray = File.ReadAllLines(DIRECTORY_LIST_FILE_PATH);
            var directoryListDataPack = new Collection<string>(allLinesArray);
            foreach (var directoryListLineData in directoryListDataPack)
            {
                AddDirectoryData(directoryListLineData);
            }
        }

        /// <summary>
        /// ディレクトリ情報を分割して追加 : SetParameter()
        /// </summary>
        /// <param name="directoryListLineData"></param>
        private void AddDirectoryData(string directoryListLineData)
        {
            var directoryDataPack = directoryListLineData.Split(',');
            RichBool isFirst = new RichBool(true);
            DirectoryData directoryData = new DirectoryData();
            foreach (var splitDirectoryData in directoryDataPack)
            {
                SetInputDataToDirectoryData(directoryData, splitDirectoryData, isFirst);
            }
            DirectoryList.Add(directoryData);
        }

        /// <summary>
        /// 初回ならばディレクトリパスに追加 : SplitDirectoryData()
        /// </summary>
        /// <param name="directryData"></param>
        /// <param name="inputData"></param>
        /// <param name="isFirst"></param>
        private void SetInputDataToDirectoryData
            (DirectoryData directryData, string inputData, RichBool isFirst)
        {
            if (isFirst)
            {
                directryData.DirectoryPath = inputData;
                isFirst.Switch();
                return;
            }
            directryData.AddSearchFormat(inputData);
        }
    }
}
