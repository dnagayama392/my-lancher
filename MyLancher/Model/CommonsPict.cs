﻿using System.Drawing; /* Icon */
using System.Drawing.Imaging; /* ImageFormat */
using System.IO; /* MemoryStream */
using System.Reflection; /* Assembly */
using System.Windows.Media.Imaging; /* BitmapImage, BitmapCacheOption */

namespace MyLancher.Model
{
    public class CommonsPict
    {
        private const string CLOCK_ICON = "MyLancher.Pics.clock.ico";
        private const string FAV_ICON   = "MyLancher.Pics.favorite.ico";
        private const string LIST_ICON  = "MyLancher.Pics.list.ico";
        private const string PIE_C_ICON = "MyLancher.Pics.pie_chart.ico";
        private const string WORLD_ICON = "MyLancher.Pics.world.ico";

        private Assembly myAssembly;

        public Assembly MyAssembly
        {
            get
            {
                return myAssembly;
            }
            set
            {
                if (myAssembly != value)
                {
                    myAssembly = value;
                }
            }
        }

        public CommonsPict()
        {
            //現在のコードを実行しているAssemblyを取得
            MyAssembly = Assembly.GetExecutingAssembly();
        }

        /// <summary>
        /// clock.ico を取得
        /// </summary>
        /// <returns></returns>
        public Icon GetClockIcon()
        {
            return GetIcon(CLOCK_ICON);
        }

        /// <summary>
        /// favorite.ico を取得
        /// </summary>
        /// <returns></returns>
        public Icon GetFavoriteIcon()
        {
            return GetIcon(FAV_ICON);
        }

        /// <summary>
        /// list.ico を取得
        /// </summary>
        /// <returns></returns>
        public Icon GetListIcon()
        {
            return GetIcon(LIST_ICON);
        }

        /// <summary>
        /// pie_chart.ico を取得
        /// </summary>
        /// <returns></returns>
        public Icon GetPieChartIcon()
        {
            return GetIcon(PIE_C_ICON);
        }

        /// <summary>
        /// world.ico を取得
        /// </summary>
        /// <returns></returns>
        public Icon GetWorldIcon()
        {
            return GetIcon(WORLD_ICON);
        }

        /// <summary>
        /// System.Drawing.Icon を System.Windows.Media.Imaging.BitmapImage に変換。
        /// </summary>
        /// <param name="icon"></param>
        /// <returns></returns>
        public BitmapImage ToBitmapImage(Icon icon)
        {
            return ToBitmapImage(icon.ToBitmap());
        }

        /// <summary>
        /// System.Drawing.Image を System.Windows.Media.Imaging.BitmapImage に変換。
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public BitmapImage ToBitmapImage(Image image)
        {
            var memory = new MemoryStream();
            image.Save(memory, ImageFormat.Png);
            memory.Position = 0;
            return ToBitmapImage(memory);
        }

        /// <summary>
        /// System.Drawing.Bitmap を System.Windows.Media.Imaging.BitmapImage に変換。
        /// </summary>
        /// <param name="bitmap"></param>
        /// <returns></returns>
        public BitmapImage ToBitmapImage(Bitmap bitmap)
        {
            var memory = new MemoryStream();
            bitmap.Save(memory, ImageFormat.Bmp);
            memory.Position = 0;
            return ToBitmapImage(memory);
        }

        /// <summary>
        /// System.IO.MemoryStream を System.Windows.Media.Imaging.BitmapImage に変換。
        /// </summary>
        /// <param name="memory"></param>
        /// <returns></returns>
        public BitmapImage ToBitmapImage(MemoryStream memory)
        {
            if (memory == null)
            {
                return null;
            }
            var bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.StreamSource = memory;
            bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
            bitmapImage.EndInit();
            return bitmapImage;
        }

        /// <summary>
        /// 指定された Icon を返す。
        /// </summary>
        /// <param name="iconName"></param>
        /// <returns></returns>
        private Icon GetIcon(string iconName)
        {
            //指定されたマニフェストリソースを読み込む
            var rs = MyAssembly.GetManifestResourceStream(iconName);
            return new System.Drawing.Icon(rs);
        }
    }
}
