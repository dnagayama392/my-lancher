﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO; /* Directory */
using System.Collections.ObjectModel; /* Collection */

namespace MyLancher.Model
{
    public class DataDirectoryManager
    {
        private const string CURRENT_PATH       = @".";
        public const string PATH_SEPARATE       = @"\";
        public const string DATA_DIRECTORY      = @"Data";
        public const string DATA_DIRECTORY_PATH = CURRENT_PATH + PATH_SEPARATE + DATA_DIRECTORY;

        private GeneralSettingFileManager settingFileManager;
        private DirectoryListFileManager directoryListFileMangager;

        private GeneralSettingFileManager SettingFileManger
        {
            get
            {
                return settingFileManager;
            }
            set
            {
                if (settingFileManager != value)
                {
                    settingFileManager = value;
                }
            }
        }

        private DirectoryListFileManager DirectoryListFileManager
        {
            get
            {
                return directoryListFileMangager;
            }
            set
            {
                if (directoryListFileMangager != value)
                {
                    directoryListFileMangager = value;
                }
            }
        }

        public DataDirectoryManager()
        {
            Initialize();
        }

        public LauncherSettingData GetSettingFileData()
        {
            return SettingFileManger.GetSettingData();
        }

        public void SetSettingFileData(LauncherSettingData newSettingData)
        {
            SettingFileManger.SetSettingData(newSettingData);
        }

        public DirectoryDataList GetDirectoryList()
        {
            return DirectoryListFileManager.GetDirectoryDataList();
        }

        public void WriteDirectoryList(DirectoryDataList newDirectoryList)
        {
            DirectoryListFileManager.Write(newDirectoryList);
        }

        private void Initialize()
        {
            CreateDataDirectory();
            SettingFileManger = new GeneralSettingFileManager();
            DirectoryListFileManager = new DirectoryListFileManager();
        }

        /// <summary>
        /// Dataディレクトリが存在しなければ作成。
        /// </summary>
        private void CreateDataDirectory()
        {
            Directory.CreateDirectory(DATA_DIRECTORY_PATH);
        }
    }
}
