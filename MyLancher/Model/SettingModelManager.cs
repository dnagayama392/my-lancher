﻿/* SettingManager.cs */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace MyLancher.Model
{
    /// <summary>
    /// 全般設定ファイルの読み書き管理
    /// </summary>
    public class SettingModelManager
    {
        private DataDirectoryManager dDirectoryManager;
        private DirectoryDataList directoryList;
        private bool defautMenuVisibility;
        private bool checkBarVisibility;
        private bool notIgnoreCase;
        private bool searchIncludeSubdirectory;
        private bool wildAvailability;

        public bool DefautMenuVisibility
        {
            get
            {
                return defautMenuVisibility;
            }
            set
            {
                if (defautMenuVisibility != value)
                {
                    defautMenuVisibility = value;
                }
            }
        }

        public bool CheckBarVisibility
        {
            get
            {
                return checkBarVisibility;
            }
            set
            {
                if (checkBarVisibility != value)
                {
                    checkBarVisibility = value;
                }
            }
        }

        public bool NotIgnoreCase
        {
            get
            {
                return notIgnoreCase;
            }
            set
            {
                if (notIgnoreCase != value)
                {
                    notIgnoreCase = value;
                }
            }
        }

        public bool SearchIncludeSubdirectory
        {
            get
            {
                return searchIncludeSubdirectory;
            }
            set
            {
                if (searchIncludeSubdirectory != value)
                {
                    searchIncludeSubdirectory = value;
                }
            }
        }

        public bool WildAvailability
        {
            get
            {
                return wildAvailability;
            }
            set
            {
                if (wildAvailability != value)
                {
                    wildAvailability = value;
                }
            }
        }

        private DataDirectoryManager DDirectoryManager
        {
            get
            {
                return dDirectoryManager;
            }
            set
            {
                if (dDirectoryManager != value)
                {
                    dDirectoryManager = value;
                }
            }
        }

        private DirectoryDataList DirectoryList
        {
            get
            {
                return directoryList;
            }
            set
            {
                if (directoryList != value)
                {
                    directoryList = value;
                }
            }
        }

        public SettingModelManager()
        {
            Initialize();
        }

        public DirectoryDataList GetDirectoryList()
        {
            return DDirectoryManager.GetDirectoryList();
        }

        public void SetSettingData()
        {
            var settingData = new LauncherSettingData();
            settingData.CheckBarVisibility        = this.CheckBarVisibility;
            settingData.DefautMenuVisibility      = this.DefautMenuVisibility;
            settingData.NotIgnoreCase             = this.NotIgnoreCase;
            settingData.SearchIncludeSubdirectory = this.SearchIncludeSubdirectory;
            settingData.WildAvailability          = this.WildAvailability;
            DDirectoryManager.SetSettingFileData(settingData);
        }

        public void WriteDirectoryList(DirectoryDataList newDirectoryList)
        {
            DDirectoryManager.WriteDirectoryList(newDirectoryList);
        }

        private void Initialize()
        {
            DDirectoryManager = new DataDirectoryManager();
            var settingData = DDirectoryManager.GetSettingFileData();
            this.CheckBarVisibility        = settingData.CheckBarVisibility;
            this.DefautMenuVisibility      = settingData.DefautMenuVisibility;
            this.NotIgnoreCase             = settingData.NotIgnoreCase;
            this.SearchIncludeSubdirectory = settingData.SearchIncludeSubdirectory;
            this.WildAvailability          = settingData.WildAvailability;
            DirectoryList = DDirectoryManager.GetDirectoryList();
        }
    }
}
