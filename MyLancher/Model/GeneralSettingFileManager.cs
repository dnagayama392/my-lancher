﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO; /* File, Directory */
using System.Collections.ObjectModel; /* Collection */

namespace MyLancher.Model
{
    /// <summary>
    /// 標準設定ファイル(Setting.csv)の入出力を扱うクラス
    /// </summary>
    public class GeneralSettingFileManager
    {
        private const string SETTING_FILE_NAME = @"general_setting.csv";
        private const string SETTING_FILE_PATH
            = DataDirectoryManager.DATA_DIRECTORY_PATH + DataDirectoryManager.PATH_SEPARATE
            + SETTING_FILE_NAME;
        public const string TAG_DEFAUT_MENU_VISIVILITY      = @"DefautMenuVisibility";
        public const string TAG_CHECK_BAR_VISIVILITY        = @"CheckBarVisibility";
        public const string TAG_NOT_IGNORE_CASE             = @"NotIgnoreCaseOfWord";
        public const string TAG_SEARCH_INCLUDE_SUBDIRECTORY = @"IsSearchIncludeSubdirectory";
        public const string TAG_WILD_AVAILABILITY           = @"WildCardAvailability";

        private LauncherSettingData settingData;

        private LauncherSettingData SettingData
        {
            get
            {
                return settingData;
            }
            set
            {
                settingData = value;
            }
        }

        public GeneralSettingFileManager()
        {
            Initialize();
        }

        public LauncherSettingData GetSettingData()
        {
            LoadSettingFile();
            return SettingData;
        }

        public void SetSettingData(LauncherSettingData newSettingData)
        {
            //if (SettingData == newSettingData)
            //{
            //    return;
            //}
            var newDefaultMenuVisibility = new RichBool(newSettingData.DefautMenuVisibility);
            var newCheckBarVisibility = new RichBool(newSettingData.CheckBarVisibility);
            var newNotIgnoreCase = new RichBool(newSettingData.NotIgnoreCase);
            var newSearchIncludeSubdirectory = new RichBool(newSettingData.SearchIncludeSubdirectory);
            var newWildAvailability = new RichBool(newSettingData.WildAvailability);
            WriteSettingData(newDefaultMenuVisibility,
                newCheckBarVisibility, newNotIgnoreCase,
                newSearchIncludeSubdirectory, newWildAvailability);
            SettingData = newSettingData;
        }

        private void Initialize()
        {
            CreateSettingFile();
            SettingData = new LauncherSettingData();
        }

        /// <summary>
        /// 設定ファイル(Setting.csv)がDataディレクトリに存在しなければ作成
        /// </summary>
        private void CreateSettingFile()
        {
            if (!Directory.Exists(DataDirectoryManager.DATA_DIRECTORY_PATH))
            {
                Directory.CreateDirectory(DataDirectoryManager.DATA_DIRECTORY_PATH);
            }
            if (!File.Exists(SETTING_FILE_PATH))
            {
                var str = "";
                str += GetStringByLineFormat("1", TAG_DEFAUT_MENU_VISIVILITY, "# (1:Visible 0:Collapsed).");
                str += GetStringByLineFormat("1", TAG_CHECK_BAR_VISIVILITY, "# (1:Visible 0:Collapsed).");
                str += GetStringByLineFormat("0", TAG_NOT_IGNORE_CASE, "# (1:NotIgnore 0:Ignore).");
                str += GetStringByLineFormat("1", TAG_SEARCH_INCLUDE_SUBDIRECTORY, "# (1:True 0:False).");
                str += GetStringByLineFormat("0", TAG_WILD_AVAILABILITY, "# (1:True 0:False).");
                File.WriteAllText(SETTING_FILE_PATH, str);
            }
        }

        /// <summary>
        /// 各設定情報を既定の形式の文字列にして返す。
        /// </summary>
        /// <param name="provisionValue"></param>
        /// <param name="provisionTag"></param>
        /// <param name="provisionSummary"></param>
        /// <returns></returns>
        private string GetStringByLineFormat(string provisionValue, string provisionTag, string provisionSummary)
        {
            return provisionValue + "," + provisionTag + "," + provisionSummary + ",x\r\n";
        }

        private void LoadSettingFile()
        {
            var allLinesArray = File.ReadAllLines(SETTING_FILE_PATH);
            var allLines = new Collection<string>(allLinesArray);
            foreach (var line in allLines)
            {
                SetParameter(line);
            }
        }

        /// <summary>
        /// 各設定パラメータの設定
        /// </summary>
        /// <param name="lineData"></param>
        private void SetParameter(string lineData)
        {
            var lineDataPack = lineData.Split(',');
            switch (lineDataPack[1])
            {
                case TAG_DEFAUT_MENU_VISIVILITY:
                    SettingData.DefautMenuVisibility = new RichBool(lineDataPack[0]).Bool;
                    break;
                case TAG_CHECK_BAR_VISIVILITY:
                    SettingData.CheckBarVisibility = new RichBool(lineDataPack[0]).Bool;
                    break;
                case TAG_NOT_IGNORE_CASE:
                    SettingData.NotIgnoreCase = new RichBool(lineDataPack[0]).Bool;
                    break;
                case TAG_SEARCH_INCLUDE_SUBDIRECTORY:
                    SettingData.SearchIncludeSubdirectory = new RichBool(lineDataPack[0]).Bool;
                    break;
                case TAG_WILD_AVAILABILITY:
                    SettingData.WildAvailability = new RichBool(lineDataPack[0]).Bool;
                    break;
                default:
                    break;
            }
        }

        private void WriteSettingData(RichBool defautMenuVisivility, RichBool checkBarVisibility,
            RichBool notIgnoreCase, RichBool searchIncludeSubDirectory, RichBool wildAvailability)
        {
            var str = "";
            str += GetStringByLineFormat(defautMenuVisivility.ToStringOfNum(), TAG_DEFAUT_MENU_VISIVILITY, "# (1:Visible 0:Collapsed).");
            str += GetStringByLineFormat(checkBarVisibility.ToStringOfNum(), TAG_CHECK_BAR_VISIVILITY, "# (1:Visible 0:Collapsed).");
            str += GetStringByLineFormat(notIgnoreCase.ToStringOfNum(), TAG_NOT_IGNORE_CASE, "# (1:NotIgnore 0:Ignore).");
            str += GetStringByLineFormat(searchIncludeSubDirectory.ToStringOfNum(), TAG_SEARCH_INCLUDE_SUBDIRECTORY, "# (1:True 0:False).");
            str += GetStringByLineFormat(wildAvailability.ToStringOfNum(), TAG_WILD_AVAILABILITY, "# (1:True 0:False).");
            File.WriteAllText(SETTING_FILE_PATH, str);
        }
    }
}
