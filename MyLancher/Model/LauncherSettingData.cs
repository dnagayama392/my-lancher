﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyLancher.Model
{
    /// <summary>
    /// 標準設定ファイルの情報を内包するためのクラス
    /// </summary>
    public class LauncherSettingData
    {
        private bool defautMenuVisibility;
        private bool checkBarVisibility;
        private bool notIgnoreCase;
        private bool searchIncludeSubDirectory;
        private bool wildAvailability;

        public bool DefautMenuVisibility
        {
            get
            {
                return defautMenuVisibility;
            }
            set
            {
                if (defautMenuVisibility != value)
                {
                    defautMenuVisibility = value;
                }
            }
        }

        public bool CheckBarVisibility
        {
            get
            {
                return checkBarVisibility;
            }
            set
            {
                if (checkBarVisibility != value)
                {
                    checkBarVisibility = value;
                }
            }
        }

        public bool NotIgnoreCase
        {
            get
            {
                return notIgnoreCase;
            }
            set
            {
                if (notIgnoreCase != value)
                {
                    notIgnoreCase = value;
                }
            }
        }

        public bool SearchIncludeSubdirectory
        {
            get
            {
                return searchIncludeSubDirectory;
            }
            set
            {
                if (searchIncludeSubDirectory != value)
                {
                    searchIncludeSubDirectory = value;
                }
            }
        }

        public bool WildAvailability
        {
            get
            {
                return wildAvailability;
            }
            set
            {
                if (wildAvailability != value)
                {
                    wildAvailability = value;
                }
            }
        }

        // コンストラクタ
        public LauncherSettingData()
        {
            Initialize();
        }

        private void Initialize()
        {
            DefautMenuVisibility      = true;
            CheckBarVisibility        = true;
            NotIgnoreCase             = false;
            SearchIncludeSubdirectory = true;
            WildAvailability          = false;
        }
    }
}
