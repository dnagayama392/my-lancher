﻿using GalaSoft.MvvmLight;
using System.Windows;
using System.Windows.Data;

namespace MyLancher.Model
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class VisibilityConverter : IValueConverter
    {
        /// <summary>
        /// Initializes a new instance of the VisibilityConverter class.
        /// </summary>
        public VisibilityConverter()
        {
        }

        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is bool))
            {
                throw new System.NotSupportedException("only bool is supported.");
            }
            if ((bool)value)
            {
                return Visibility.Visible;
            }
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new System.NotImplementedException();
        }
    }
}