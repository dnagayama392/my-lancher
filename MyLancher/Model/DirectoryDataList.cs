﻿/* DirectoryDataList.cs */
using System.Collections.ObjectModel; /* Collection */
using System.IO; /* Directory, File, SearchOption */
using System.Linq;
using System.Collections.Generic;

namespace MyLancher.Model
{
    public class DirectoryDataList
    {
        private Collection<DirectoryData> directoryList;

        private Collection<DirectoryData> DirectoryList
        {
            get
            {
                return directoryList;
            }
            set
            {
                directoryList = value;
            }
        }

        public DirectoryDataList()
        {
            Initialize();
        }

        public DirectoryDataList(Collection<DirectoryData> directoryDataList)
        {
            DirectoryList = directoryDataList;
        }

        public void Add(DirectoryData additionalData)
        {
            DirectoryList.Add(additionalData);
        }

        public void AddPattern(string additionalPattern, string directoryPath)
        {
            var targetDirectoryData = GetDirectoryDataInList(directoryPath);
            var dataIndex = DirectoryList.IndexOf(targetDirectoryData);
            DirectoryList[dataIndex].DirectoryMatchFormat.Add(additionalPattern);
        }

        public void ReadFile() // TODO: indexfileクラスをつくり移管
        {
            var allLinesAry = File.ReadAllLines(DirectoryListFileManager.DIRECTORY_LIST_FILE_PATH);
            var directoryListDataPack = new Collection<string>(allLinesAry);
            foreach (var directoryListLineData in directoryListDataPack)
            {
                ReadDirectoryData(directoryListLineData);
            }
        }
        
        public void Clear()
        {
            DirectoryList.Clear();
        }

        public Collection<DirectoryData> GetCollectionList()
        {
            return DirectoryList;
        }

        /// <summary>
        /// 入力されたディレクトリパスに対応するファイル検索のパターンを返す。
        /// </summary>
        /// <param name="directoryPath">ディレクトリのフルパス</param>
        /// <returns>検索パターンのコレクション(Collection＜string＞)</returns>
        public Collection<string> GetMatchingPattern(string directoryPath)
        {
            var directoryData = GetDirectoryDataInList(directoryPath);
            if (directoryData != null)
            {
                return directoryData.DirectoryMatchFormat;
            }
            return null;
        }

        public Collection<string> GetFilePaths(SearchOption searchOption)
        {
            Collection<string> filePathList = new Collection<string>();
            foreach (var directorydata in DirectoryList)
            {
                var additionalFilePathList = GetFilePathPerDirectory(directorydata, searchOption);
                filePathList = MargeCollection(filePathList, additionalFilePathList);
            }
            return filePathList;
        }

        public void RemoveDerectoryPath(string targetDirectoryPath)
        {
            var deletedDirectoryData = GetDirectoryDataInList(targetDirectoryPath);
            if (deletedDirectoryData == null)
            {
                return;
            }
            DirectoryList.Remove(deletedDirectoryData);
        }

        public void RemovePattern(string targetPattern, string directoryPath)
        {
            var targetDirectoryData = GetDirectoryDataInList(directoryPath);
            if (targetDirectoryData == null)
            {
                return;
            }
            var dataIndex = DirectoryList.IndexOf(targetDirectoryData);
            DirectoryList[dataIndex].DirectoryMatchFormat.Remove(targetPattern);
        }

        /// <summary>
        /// ディレクトリパスからリスト内のディレクトリ情報を取得
        /// </summary>
        /// <param name="targetDirectoryPath"></param>
        /// <returns></returns>
        private DirectoryData GetDirectoryDataInList(string targetDirectoryPath)
        {
            foreach (var directoryData in DirectoryList)
            {
                if (directoryData.DirectoryPath == targetDirectoryPath)
                {
                    return directoryData;
                }
            }
            return null;
        }

        /// <summary>
        /// ディレクトリ下のファイルの一覧を検索パターン毎に取得
        /// </summary>
        /// <param name="directorydata"></param>
        /// <param name="searchOption"></param>
        /// <returns></returns>
        private Collection<string> GetFilePathPerDirectory(DirectoryData directorydata, SearchOption searchOption)
        {
            Collection<string> filePathList = new Collection<string>();
            foreach (var matchPattern in directorydata.DirectoryMatchFormat)
            {
                var additionalFilePathList = GetFiles(directorydata.DirectoryPath, matchPattern, searchOption);
                filePathList = MargeCollection(filePathList, additionalFilePathList);
            }
            return filePathList;
        }

        /// <summary>
        /// ディレクトリ下で検索パターンに一致したファイルのパス一覧を返す
        /// </summary>
        /// <param name="directoryPath"></param>
        /// <param name="searchPattern"></param>
        /// <param name="searchOption"></param>
        /// <returns></returns>
        private Collection<string> GetFiles(string directoryPath, string searchPattern, SearchOption searchOption)
        {
            return new Collection<string>(Directory.GetFiles(directoryPath, searchPattern, searchOption));
        }

        /// <summary>
        /// 前者のコレクションに後者のコレクションをマージ
        /// </summary>
        /// <param name="collecttion1"></param>
        /// <param name="collecttion2"></param>
        /// <returns></returns>
        private Collection<string> MargeCollection(Collection<string> collecttion1, Collection<string> collecttion2)
        {
            foreach (var str in collecttion2)
            {
                collecttion1.Add(str);
            }
            return collecttion1;
        }

        /// <summary>
        /// ディレクトリ情報を読んで追加
        /// </summary>
        /// <param name="directoryListLineData"></param>
        private void ReadDirectoryData(string directoryListLineData)
        {
            var directoryDataPack = directoryListLineData.Split(',');
            RichBool isFirst = new RichBool(true);
            DirectoryData directoryData = new DirectoryData();
            foreach (var splitDirectoryData in directoryDataPack)
            {
                SetInputDataToDirectoryData(directoryData, splitDirectoryData, isFirst);
            }
            Add(directoryData);
        }

        private void SetInputDataToDirectoryData
            (DirectoryData directryData, string inputData, RichBool isFirst)
        {
            if (isFirst)
            {
                directryData.DirectoryPath = inputData;
                isFirst.Switch();
                return;
            }
            directryData.AddSearchFormat(inputData);
        }

        private void Initialize()
        {
            DirectoryList = new ObservableCollection<DirectoryData>();
        }
    }
}
