﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel; /* Collection */

namespace MyLancher.Model
{
    public class DirectoryData
    {
        private string directoryPath;
        private Collection<string> directoryMatchFormat;

        public string DirectoryPath
        {
            get
            {
                return directoryPath;
            }
            set
            {
                directoryPath = value;
            }
        }

        public Collection<string> DirectoryMatchFormat
        {
            get
            {
                return directoryMatchFormat;
            }
            set
            {
                directoryMatchFormat = value;
            }
        }

        public DirectoryData()
        {
            directoryPath = null;
            directoryMatchFormat = new Collection<string>();
        }

        public void AddSearchFormat(string format)
        {
            DirectoryMatchFormat.Add(format);
        }
    }
}
