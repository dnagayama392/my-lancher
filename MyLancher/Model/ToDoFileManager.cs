﻿/* ToDoFileManager.cs */
using System.IO; /* File */
using System.Collections.ObjectModel; /* Collection */

namespace MyLancher.Model
{
    /// <summary>
    /// ファイル ToDoList.csv への入出力を管理
    /// </summary>
    public class ToDoFileManager
    {
        private const string TODO_LIST_FILE_NAME = @"todo_list.csv";
        private const string TODO_LIST_FILE_PATH
            = DataDirectoryManager.DATA_DIRECTORY_PATH
            + DataDirectoryManager.PATH_SEPARATE + TODO_LIST_FILE_NAME;
        private const string TODO_LIST_FILE_HEADER
            = "#ToDoTitle,Content,ExecuteState,CheckState,ExexuteValue,x\r\n";

        public ToDoFileManager()
        {
            Initialize();
        }

        /// <summary>
        /// ファイルから読み出したToDoリストを渡します。
        /// </summary>
        /// <returns></returns>
        public Collection<NotifyToDoData> Read()
        {
            return ReadToDoFile();
        }

        /// <summary>
        /// ToDoリストをファイルに記入します。
        /// </summary>
        public void Write(Collection<NotifyToDoData> newToDoList)
        {
            WriteToDoFile(newToDoList);
        }

        private void Initialize()
        {
            if (!File.Exists(TODO_LIST_FILE_PATH))
            {
                CreateToDoFile();
            }
        }

        private void CreateToDoFile()
        {
            string fileContent ="";
            fileContent += TODO_LIST_FILE_HEADER;
            File.AppendAllText(TODO_LIST_FILE_PATH, fileContent);
        }

        private Collection<NotifyToDoData> ReadToDoFile()
        {
            var list = new Collection<NotifyToDoData>();
            var isFirst = new RichBool(true);
            var allLines = File.ReadAllLines(TODO_LIST_FILE_PATH);
            foreach (var lineData in allLines)
            {
                var additionalToDoData = ConvertToNotifyToDoData(lineData, isFirst);
                if (additionalToDoData != null)
                {
                    list.Add(additionalToDoData);
                }
            }
            return list;
        }

        private NotifyToDoData ConvertToNotifyToDoData(string lineData, RichBool isFirst)
        {
            if (isFirst.Bool)
            {
                isFirst.False();
                return null;
            }
            var toDoDataPack = lineData.Split(',');
            var headerPack   = TODO_LIST_FILE_HEADER.Split(',');
            if (toDoDataPack.Length != headerPack.Length)
            {
                return null;
            }
            var newTitle        = ReplaceTextFromFile(toDoDataPack[0].Trim('"'));
            var newContent      = ReplaceTextFromFile(toDoDataPack[1].Trim('"'));
            var newExecuteState = new RichBool(toDoDataPack[2]).Bool;
            var newCheckState   = new RichBool(toDoDataPack[3]).Bool;
            var newExecuteValue = int.Parse(toDoDataPack[4]);
            if (newExecuteValue < 0 || newExecuteValue > 100)
            {
                newExecuteValue = 0;
            }
            return new NotifyToDoData(newTitle, newContent, newExecuteState, newCheckState, newExecuteValue);
        }

        private void WriteToDoFile(Collection<NotifyToDoData> newToDoList)
        {
            var fileContents = TODO_LIST_FILE_HEADER;
            foreach (var newToDo in newToDoList)
            {
                var newTitle   = ReplaceTextToFile(newToDo.Title);
                fileContents += "\"" + newTitle + "\"" + ",";
                var newContent = ReplaceTextToFile(newToDo.Content);
                fileContents += "\"" + newContent + "\"" + ",";
                fileContents += new RichBool(newToDo.ExecuteState).ToStringOfNum() + ",";
                fileContents += new RichBool(newToDo.CheckState).ToStringOfNum() + ",";
                fileContents += newToDo.ExecuteValue.ToString() + ",";
                fileContents += "x\r\n";
            }
            File.WriteAllText(TODO_LIST_FILE_PATH, fileContents);
        }

        private string ReplaceTextFromFile(string str)
        {
            str = str.Replace("@r@n", "\r\n");
            str = str.Replace("@x2C", ",");
            return str;
        }

        private string ReplaceTextToFile(string str)
        {
            str = str.Replace("\r\n", "@r@n");
            str = str.Replace(",", "@x2C");
            return str;
        }
    }
}
