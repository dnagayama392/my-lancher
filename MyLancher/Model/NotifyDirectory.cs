﻿/* NotifyDirectory.cs */
using System; /* String */
using System.ComponentModel; /* INotifyPropertyChanged */

namespace MyLancher.Model
{
    /// <summary>
    /// カタログの設定タブの検索対象ディレクトリ一覧表示のためのクラス
    /// </summary>
    public class NotifyDirectory : INotifyPropertyChanged
    {
        private string _path;

        public string Path
        {
            get
            {
                return _path;
            }
            set
            {
                if (_path != value)
                {
                    _path = value;
                    NotifyPropertyChanged("Path");
                }
            }
        }

        public NotifyDirectory(string path)
        {
            Path = path;
        }

        public override string ToString()
        {
            return Path;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
