﻿/* AlarmModelManager.cs */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel; /* ObservableCollection */
using MyLancher.ViewModel; /* AlarmListViewModel */

namespace MyLancher.Model
{
    /// <summary>
    /// AlarmViewModel.cs が煩雑にならないように、委譲できるものはこちらへ
    /// </summary>
    public class AlarmModelManager
    {
        private AlarmFileManager alarmManager;
        private Collection<AlarmListViewModel> alarmDataCollection;
        private DateTimeManager nowTime;

        private AlarmFileManager AlarmManager
        {
            get
            {
                return alarmManager;
            }
            set
            {
                if (alarmManager != value)
                {
                    alarmManager = value;
                }
            }
        }

        public Collection<AlarmListViewModel> AlarmDataCollection
        {
            get
            {
                return alarmDataCollection;
            }
            set
            {
                if (alarmDataCollection != value)
                {
                    alarmDataCollection = value;
                }
            }
        }

        private DateTimeManager NowTime
        {
            get
            {
                return nowTime;
            }
            set
            {
                if (nowTime != value)
                {
                    nowTime = value;
                }
            }
        }

        public string GetStrCurrentTime()
        {
            return "現在時刻\r\n   " + NowTime.ToStrFullTime();
        }

        public int CurrentMonth
        {
            get
            {
                return NowTime.Month;
            }
        }

        public int CurrentDay
        {
            get
            {
                return NowTime.Day;
            }
        }

        public int CurrentHour
        {
            get
            {
                return NowTime.Hour;
            }
        }

        public int CurrentMinute
        {
            get
            {
                return NowTime.Minute;
            }
        }

        public AlarmModelManager()
        {
            Initialize();
        }

        public void ResetTime()
        {
            NowTime.SetNow();
        }

        public void ReadAlarm()
        {
            AlarmDataCollection = AlarmManager.Read();
            TrimBeforeNow();
            SortValidation();
        }

        public void AddAlarm(AlarmListViewModel newAlarm)
        {
            AlarmDataCollection.Add(newAlarm);
        }

        public void WriteAlarm()
        {
            AlarmManager.Write(AlarmDataCollection);
        }

        public long GetNowTimeValue()
        {
            return NowTime.ToTimeValue();
        }

        private void Initialize()
        {
            AlarmManager = new AlarmFileManager();
            AlarmDataCollection = new Collection<AlarmListViewModel>();
            NowTime = new DateTimeManager();
            NowTime.SetNow();
            ReadAlarm();
        }

        private void SortValidation()
        {
            if (AlarmDataCollection.Count < 1)
            {
                return;
            }
            var validedAlarm = new Collection<AlarmListViewModel>();
            var invalidedAlarm = new Collection<AlarmListViewModel>();
            foreach (var alarm in AlarmDataCollection)
            {
                if (alarm.Validation == AlarmListViewModel.VALID_TEXT)
                {
                    validedAlarm.Add(alarm);
                }
                if (alarm.Validation == AlarmListViewModel.INVALID_TEXT)
                {
                    invalidedAlarm.Add(alarm);
                }
            }
            AlarmDataCollection.Clear();
            foreach (var alarm in validedAlarm)
            {
                AlarmDataCollection.Add(alarm);
            }
            foreach (var alarm in invalidedAlarm)
            {
                AlarmDataCollection.Add(alarm);
            }
        }

        private void TrimBeforeNow()
        {
            var nowTimeValue = GetNowTimeValue();
            for (var index = 0; index < AlarmDataCollection.Count; index++)
            {
                if (AlarmDataCollection[index].GetNextTimeValue() < nowTimeValue)
                {
                    AlarmDataCollection.RemoveAt(index);
                    index--;
                }
            }
        }
    }
}
