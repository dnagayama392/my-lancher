﻿/* LauncherModelManager.cs */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel; /* ObservableCollection */
using MyLancher.ViewModel; /* SelectableItemViewModel */

namespace MyLancher.Model
{
    public class LauncherModelManager
    {
        private ObservableCollection<SelectableItemViewModel> visibleItemList;
        private IndexListManager indexManager;
        private GeneralSettingFileManager settingFileManager;
        private SearchedItemList matchedItemList;
        private SearchedItemList defaultMenuList;
        private int visibleItemCount;
        private bool defaultMenuVisibleState;

        public ObservableCollection<SelectableItemViewModel> VisibleItemList
        {
            get
            {
                return visibleItemList;
            }
            set
            {
                if (visibleItemList != value)
                {
                    visibleItemList = value;
                }
            }
        }

        public IndexListManager IndexManager
        {
            get
            {
                return indexManager;
            }
            set
            {
                if (indexManager != value)
                {
                    indexManager = value;
                }
            }
        }

        public GeneralSettingFileManager SettingFileManager
        {
            get
            {
                return settingFileManager;
            }
            set
            {
                if (settingFileManager != value)
                {
                    settingFileManager = value;
                }
            }
        }

        public SearchedItemList MatchedItemList
        {
            get
            {
                return matchedItemList;
            }
            set
            {
                if (matchedItemList != value)
                {
                    matchedItemList = value;
                }
            }
        }

        public SearchedItemList DefaultItemList
        {
            get
            {
                return defaultMenuList;
            }
            set
            {
                if (defaultMenuList != value)
                {
                    defaultMenuList = value;
                }
            }
        }

        public int VisibleItemCount
        {
            get
            {
                return visibleItemCount;
            }
            set
            {
                if (visibleItemCount != value)
                {
                    visibleItemCount = value;
                }
            }
        }

        public bool DefaultMenuVisibleState
        {
            get
            {
                return defaultMenuVisibleState;
            }
            set
            {
                if (defaultMenuVisibleState != value)
                {
                    defaultMenuVisibleState = value;
                    SetDefautMenuList();
                }
            }
        }

        public LauncherModelManager()
        {
            Initialize();
        }

        /// <summary>
        /// 検索結果から指定したインデックスのアイテムを取得する。
        /// 
        /// 例外
        ///   System.ArgumentException
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public SearchedItem GetMatchItemIndexOf(int index)
        {
            if (index < 0)
            {
                throw new System.ArgumentException();
            }
            if (index >= MatchedItemList.Count)
            {
                throw new System.ArgumentException();
            }
            return MatchedItemList.GetItemDataIndexOf(index);
        }

        public void Search(string inputText)
        {
            MatchedItemList = IndexManager.GetTargetItemList(inputText);
        }

        public void SetSettingStatus(LauncherSettingData settingData)
        {
            IndexManager.SetSettingStatus(settingData);//HACK: index
        }

        public void SetDefautMenuList()
        {
            DefaultItemList = new SearchedItemList();
            if (DefaultMenuVisibleState)
            {
                DefaultItemList.SetDefaultMenu();
            }
        }

        public void SetVisibleItemList()
        {
            AddItemListToVisibleItemList(MatchedItemList);
            SetItemCount();

            if (DefaultItemList.Count > 0)
            {
                RemoveItemListToVisibleItemList(DefaultItemList);
                AddItemListToVisibleItemList(DefaultItemList);
            }
        }

        public void ResetIndexManager()
        {
            IndexManager = new IndexListManager();
        }

        private void Initialize()
        {
            VisibleItemList = new ObservableCollection<SelectableItemViewModel>();
            DefaultItemList = new SearchedItemList();
            MatchedItemList = new SearchedItemList();
            IndexManager    = new IndexListManager();
        }

        private void AddItemListToVisibleItemList(SearchedItemList itemList)
        {
            var itemCollections = itemList.GetItemList();
            foreach (var itemData in itemCollections)
            {
                var itemDataVM = new SelectableItemViewModel(itemData);
                VisibleItemList.Add(itemDataVM);
            }
        }

        private void SetItemCount()
        {
            var additionalItemCount = MatchedItemList.Count;
            var defaultItemCount = 0;
            if (DefaultItemList.Count > 0)
            {
                defaultItemCount += DefaultItemList.Count;
            }
            VisibleItemCount = additionalItemCount + defaultItemCount;
        }

        private void RemoveItemListToVisibleItemList(SearchedItemList itemList)
        {
            var itemCollections = itemList.GetItemList();
            foreach (var itemData in itemCollections)
            {
                var itemDataVM = new SelectableItemViewModel(itemData);
                for(var i=0;i<VisibleItemList.Count;i++)
                {
                    if (itemData.ItemPath == VisibleItemList[i].ItemPath)
                    {
                        VisibleItemList.RemoveAt(i);
                        VisibleItemCount--;
                    }
                }
            }
        }
    }
}
