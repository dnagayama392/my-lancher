﻿using System.Windows.Media.Imaging; /* BitmapImage */
using System.Collections.ObjectModel; /* Collection */

namespace MyLancher.Model
{
    class BitmapImageList
    {
        private Collection<BitmapImage> bmpImageList;

        private Collection<BitmapImage> BMPImageList
        {
            get
            {
                return bmpImageList;
            }
            set
            {
                bmpImageList = value;
            }
        }

        public int Count // HACK: 未使用状態
        {
            get
            {
                return BMPImageList.Count;
            }
        }

        public BitmapImage this[int index]
        {
            get
            {
                return BMPImageList[index];
            }
            set
            {
                BMPImageList[index] = value;
            }
        }

        public BitmapImageList()
        {
            Initialize();
        }

        public void Add(BitmapImage bmpImage)
        {
            BMPImageList.Add(bmpImage);
        }

        public bool Contains(BitmapImage bmpImage)
        {
            return BMPImageList.Contains(bmpImage);
        }

        private void Initialize()
        {
            BMPImageList = new Collection<BitmapImage>();
        }
    }
}
