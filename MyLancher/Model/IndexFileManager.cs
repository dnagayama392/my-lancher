﻿/* IndexFileManager.cs */
using System.IO; /* File, Path */

namespace MyLancher.Model
{
    public class IndexFileManager
    {
        private const string INDEX_FILE_NAME = @"index.csv";
        private const string INDEX_FILE_PATH
            = DataDirectoryManager.DATA_DIRECTORY_PATH + DataDirectoryManager.PATH_SEPARATE
            + INDEX_FILE_NAME;
        private const int INDEX_FILE_ELEMENTS = 2;

        public IndexFileManager()
        {
            Initialize();
        }

        public int GetNumberOfItems()
        {
            var allLines = File.ReadAllLines(INDEX_FILE_PATH);
            if (allLines.Length <= 0)
            {
                return 0;
            }
            var countData = allLines[0].Split(',');
            if (countData.Length <= 0)
            {
                return 0;
            }
            return int.Parse(countData[0]);
        }

        public SearchedItemList ReadIndexList()
        {
            var list = new SearchedItemList();
            var isFirst = new RichBool(true);
            var allLines = File.ReadAllLines(INDEX_FILE_PATH);
            foreach (var lineText in allLines)
            {
                AddList(list, lineText, isFirst);
            }
            return list;
        }

        /// <summary>
        /// IndexFileを再設定（再作成）します
        /// </summary>
        public void Remake()
        {
            CreateIndexFile();
        }

        private void Initialize()
        {
            if (!File.Exists(INDEX_FILE_PATH))
            {
                CreateIndexFile();
            }
        }

        private void AddList(SearchedItemList list, string lineText, RichBool isFirst)
        {
            if (isFirst.Bool)
            {
                var indexPack = lineText.Split(',');
                isFirst.False();
                return;
            }
            var itemData = GetItemData(lineText);
            if (itemData != null)
            {
                list.Add(GetItemData(lineText));
            }
        }

        private SearchOption ConvertToSearchOption(bool b)
        {
            return b ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
        }

        private void CreateIndexFile()
        {
            var directoryListManager = new DirectoryListFileManager();
            var targetDirectoryList = directoryListManager.GetDirectoryDataList();
            var settingData = new GeneralSettingFileManager().GetSettingData();
            var searchType = ConvertToSearchOption(settingData.SearchIncludeSubdirectory);
            var filePaths = targetDirectoryList.GetFilePaths(searchType);
            var fileText = "";
            var count = filePaths.Count;
            fileText += count.ToString() + ",#Number of item(s),x\r\n";
            foreach (var filepath in filePaths)
            {
                fileText += Path.GetFileNameWithoutExtension(filepath) + ",";
                fileText += filepath + ",";
                fileText += "x\r\n";
            }
            File.WriteAllText(INDEX_FILE_PATH, fileText);
        }

        private SearchedItem GetItemData(string lineText)
        {
            var indexPack = lineText.Split(',');
            if (indexPack.Length != INDEX_FILE_ELEMENTS + 1)
            {
                return null;
            }
            return new SearchedItem(indexPack[0], indexPack[1]);
        }
    }
}
