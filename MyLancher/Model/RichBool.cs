﻿using System.Diagnostics; /* Debug */

namespace MyLancher.Model
{
    /// <summary>
    /// boolにTrue,Falseの入れ替え機構が欲しかったので(…探せばありそう)
    /// </summary>
    public class RichBool
    {
        private bool b;

        public bool Bool
        {
            get
            {
                return b;
            }
            set
            {
                b = value;
            }
        }

        public RichBool()
        {
            Bool = false; /* HACK: boolの規定値なので不要かも */
        }

        public RichBool(bool b)
        {
            Bool = b; 
        }

        public RichBool(int i)
        {
            Bool = IntToBool(i);
        }

        public RichBool(string str)
        {
            Bool = StringToBool(str);
        }

        public RichBool(object obj)
        {
            if (obj is bool)
            {
                Bool = (bool)obj;
            }
            if (obj is int)
            {
                Bool = IntToBool((int)obj);
            }
            if (obj is string)
            {
                Bool = StringToBool((string)obj);
            }
            throw new System.NotSupportedException("RichBool are supported only bool and int.");
        }

        public static bool operator true(RichBool rb)
        {
            return rb.Bool != false; // HACK: 比較演算子不要かも 要確認
        }

        public static bool operator false(RichBool rb)
        {
            return rb.Bool == false;
        }

        public static bool operator !(RichBool rb)
        {
            return !rb.Bool;
        }

        public void True()
        {
            Bool = true;
        }

        public void False()
        {
            Bool = false;
        }

        /// <summary>
        /// trueとfalseを入れ替える
        /// </summary>
        public void Switch()
        {
            Bool = !Bool;
        }

        public string ToStringOfNum()
        {
            return Bool ? "1" : "0";
        }

        private bool IntToBool(int i)
        {
            return (i != 0);
        }

        private bool StringToBool(string str)
        {
            if (IsIntTryParse(str))
            {
                return IntToBool(int.Parse(str));
            }
            Debug.Assert(false, "this strint can't change to int.");
            throw new System.NotSupportedException("this string is not supported.");
        }

        private bool IsIntTryParse(string str)
        {
            int notused = 0;
            return int.TryParse(str, out notused);
        }
    }
}
