﻿/* ItemDataList.cs */
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel; /* ObservableCollection */

namespace MyLancher.Model
{
    public class SearchedItemList
    {
        private StringList itemName;
        private StringList itemPath;
        private BitmapImageList iconImage;

        private StringList ItemName
        {
            get
            {
                return itemName;
            }
            set
            {
                if (itemName != value)
                {
                    itemName = value;
                }
            }
        }

        private StringList ItemPath
        {
            get
            {
                return itemPath;
            }
            set
            {
                if (itemPath != value)
                {
                    itemPath = value;
                }
            }
        }

        private BitmapImageList IconImage
        {
            get
            {
                return iconImage;
            }
            set
            {
                if (iconImage != value)
                {
                    iconImage = value;
                }
            }
        }

        public int Count
        {
            get
            {
                //return ItemList.Count;
                if (ItemPath.Count == ItemName.Count)
                {
                    return ItemPath.Count;
                }
                return -1;
            }
        }

        public SearchedItemList()
        {
            Initialize();
        }

        public void Add(SearchedItem additionalItem)
        {
            if (ItemPath.Contains(additionalItem.ItemPath))
            {
                return;
            }
            ItemName.Add(additionalItem.ItemName);
            ItemPath.Add(additionalItem.ItemPath);
            IconImage.Add(additionalItem.IconBitmapImage);
        }

        public void Marge(SearchedItemList additionalItemList)
        {
            var numberOfItem = additionalItemList.Count;
            for (var loop = 0; loop < numberOfItem; loop++)
            {
                Add(additionalItemList.GetItemDataIndexOf(loop));
            }
        }

        public SearchedItem GetItemDataIndexOf(int index)
        {
            return new SearchedItem(ItemName[index], ItemPath[index], IconImage[index]);
        }

        public SearchedItem GetItemDataNameOf(string name)
        {
            var itemIndex = ItemName.IndexOf(name);
            return GetItemDataIndexOf(itemIndex);
        }

        public ObservableCollection<SearchedItem> GetItemList()
        {
            ObservableCollection<SearchedItem> itemList;
            itemList = new ObservableCollection<SearchedItem>();

            for (var index = 0; index < Count; index++)
            {
                itemList.Insert(index, GetItemDataIndexOf(index));
            }

            return itemList;
        }

        public StringList GetItemNameList()
        {
            return ItemName;
        }

        public void SetDefaultMenu()
        {
            Clear();
            var comPic = new CommonsPict();
            // 最初に入力したものから順に、上から表示される(ようにMainViewModel.csを記述)
            // DONE: 絶対パスの排除
            var todoIcon = comPic.GetListIcon();
            var todoBitmapImage = comPic.ToBitmapImage(todoIcon);
            var todo = new SearchedItem("ToDo List", "ToDo", todoBitmapImage);
            Add(todo);
            var alarmIcon = comPic.GetClockIcon();
            var alarmBitmapImage = comPic.ToBitmapImage(alarmIcon);
            var alarm = new SearchedItem("Alarm", "Alarm", alarmBitmapImage);
            Add(alarm);
            //var bookmarkIcon = comPic.GetFavoriteIcon();
            //var bookmarkBitmapImage = comPic.ToBitmapImage(bookmarkIcon);
            //var bookmark = new SearchedItem("Bookmark", "Bookmark", bookmarkBitmapImage);
            //Add(boolmark);
            //var cpuIcon = comPic.GetPieChartIcon();
            //var cpuBitmapImage = comPic.ToBitmapImage(cpuIcon);
            //var cpu = new SearchedItem("CPU Status", "CPU Status", cpuBitmapImage);
            //Add(cpu);
        }

        public void Clear()
        {
            Initialize();
        }

        private void Initialize()
        {
            //ItemList = new ObservableCollection<ItemData>();
            ItemName = new StringList();
            ItemPath = new StringList();
            IconImage = new BitmapImageList();
        }
    }
}
