﻿/* ItemData.cs */
using System;
using System.Collections.Generic;
using System.Windows.Media.Imaging; /* BitmapImage, BitmapCacheOption */
using System.Drawing; /* Icon, Image */
using System.Drawing.Imaging; /* ImageFormat */
using System.IO; /* MemoryStream, Path */

namespace MyLancher.Model
{
    /// <summary>
    /// ひとつのアイテムが保持する情報
    /// </summary>
    public class SearchedItem
    {
        private const string KEY_TODO = "ToDo";
        private const string KEY_ALARM = "Alarm";

        private string itemName;
        private string itemPath;
        private BitmapImage iconBitmapImage;

        public string ItemName
        {
            get
            {
                return itemName;
            }
            set
            {
                if (itemName != value)
                {
                    itemName = value;
                }
            }
        }

        public string ItemPath
        {
            get
            {
                return itemPath;
            }
            set
            {
                if (itemPath != value)
                {
                    itemPath = value;
                }
            }
        }

        public BitmapImage IconBitmapImage
        {
            get
            {
                return iconBitmapImage;
            }
            set
            {
                if (iconBitmapImage != value)
                {
                    iconBitmapImage = value;
                }
            }
        }

        public SearchedItem(string strItemPath)
        {
            var strItemName = Path.GetFileNameWithoutExtension(strItemPath);
            var bmpImgOfIcon = GetIconBitmapImageFromfilePath(strItemPath);
            Initialize(strItemName, strItemPath, bmpImgOfIcon);
        }

        public SearchedItem(string strItemName, string strItemPath)
        {
            var bmpImgOfIcon = GetIconBitmapImageFromfilePath(strItemPath);
            Initialize(strItemName, strItemPath, bmpImgOfIcon);
        }

        public SearchedItem(string strItemName, string strItemPath, BitmapImage bmpImgOfIcon)
        {
            Initialize(strItemName, strItemPath, bmpImgOfIcon);
        }

        public SearchedItem(string strItemName, string strItemPath, string bmpImagePath)
        {
            var bmpImgOfIcon = new BitmapImage(new Uri(bmpImagePath));
            Initialize(strItemName, strItemPath, bmpImgOfIcon);
        }

        public void Execute()
        {
            if (ItemPath == KEY_TODO)
            {
                var todoWindow = new ToDoWindow();
                todoWindow.Show();
                return;
            }
            if (ItemPath == KEY_ALARM)
            {
                var todoWindow = new AlarmWindow();
                todoWindow.Show();
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(ItemPath);
            }
            catch (System.IO.FileNotFoundException)
            {
                System.Windows.Forms.MessageBox.Show("対象のファイルは存在しません。カタログを再設定してください。");
                return;
            }
            catch (System.ObjectDisposedException)
            {
                return;
            }
            catch (System.ComponentModel.Win32Exception)
            {
                return;
            }
        }

        private void Initialize(string strItemName, string strItemPath, BitmapImage bmpImgOfIcon)
        {
            ItemName = strItemName;
            ItemPath = strItemPath;
            IconBitmapImage = bmpImgOfIcon;
        }

        private BitmapImage GetIconBitmapImageFromfilePath(string filepath)
        {
            Icon appIcon = Icon.ExtractAssociatedIcon(filepath);
            Image image = appIcon.ToBitmap();
            var comPic = new CommonsPict();
            return comPic.ToBitmapImage(image);
        }
    }
}
