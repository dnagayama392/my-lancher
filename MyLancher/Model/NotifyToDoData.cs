﻿/* NotifyToDoData.cs */
using System; /* String */
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel; /* INotifyPropertyChanged, PropertyChangedEventHandler */
using GalaSoft.MvvmLight.Command; /* RelayCommand */
using System.Windows; /* Visibility */
//using System.Windows.Media; /* Brush */

namespace MyLancher.Model
{
    public class NotifyToDoData : INotifyPropertyChanged
    {
        private string title;
        private string content;
        private bool executeState;
        private bool checkState;
        private int executeValue;
        private string executeIconPath;
        private string checkIconPath;
        private Visibility executeButtonVisibility;
        private Visibility executeValueVisibility;
        private RelayCommand changeExecuteState;
        private RelayCommand changeCheckState;

        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                if (title != value)
                {
                    title = value;
                    NotifyPropertyChanged("Title");
                }
            }
        }

        public string Content
        {
            get
            {
                return content;
            }
            set
            {
                if (content != value)
                {
                    content = value;
                    NotifyPropertyChanged("Content");
                }
            }
        }

        public bool ExecuteState
        {
            get
            {
                return executeState;
            }
            set
            {
                if (executeState != value)
                {
                    executeState = value;
                    UpdateExecuteIconImage();
                    NotifyPropertyChanged("ExecuteState");
                }
            }
        }

        public bool CheckState
        {
            get
            {
                return checkState;
            }
            set
            {
                if (checkState != value)
                {
                    checkState = value;
                    UpdateByCheckState();
                    NotifyPropertyChanged("CheckState");
                }
            }
        }

        public int ExecuteValue
        {
            get
            {
                return executeValue;
            }
            set
            {
                if (executeValue != value)
                {
                    executeValue = value;
                    NotifyPropertyChanged("ExecuteValue");
                }
            }
        }

        public string ExecuteIconPath
        {
            get
            {
                return executeIconPath;
            }
            set
            {
                if (executeIconPath != value)
                {
                    executeIconPath = value;
                    NotifyPropertyChanged("ExecuteIconPath");
                }
            }
        }

        public string CheckIconPath
        {
            get
            {
                return checkIconPath;
            }
            set
            {
                if (checkIconPath != value)
                {
                    checkIconPath = value;
                    NotifyPropertyChanged("CheckIconPath");
                }
            }
        }

        public Visibility ExecuteButtonVisibility
        {
            get
            {
                return executeButtonVisibility;
            }
            set
            {
                if (executeButtonVisibility != value)
                {
                    executeButtonVisibility = value;
                    NotifyPropertyChanged("ExecuteButtonVisibility");
                }
            }
        }

        public Visibility ExecuteValueVisibility
        {
            get
            {
                return executeValueVisibility;
            }
            set
            {
                if (executeValueVisibility != value)
                {
                    executeValueVisibility = value;
                    NotifyPropertyChanged("ExecuteValueVisibility");
                }
            }
        }

        public RelayCommand ChangeExecuteState
        {
            get
            {
                if (changeExecuteState == null)
                {
                    changeExecuteState = new RelayCommand(() =>
                    {
                        ExecuteState = !ExecuteState;
                        UpdateByExecuteState();
                    });
                }
                return changeExecuteState;
            }
        }

        public RelayCommand ChangeCheckState
        {
            get
            {
                if (changeCheckState == null)
                {
                    changeCheckState = new RelayCommand(() =>
                    {
                        //System.Windows.Forms.MessageBox.Show("CheckStateを変更します！");
                        CheckState = !CheckState;
                        UpdateByCheckState();
                    });
                }
                return changeCheckState;
            }
        }

        public NotifyToDoData()
        {
            Initialize("", "", false, false, 0);
        }

        public NotifyToDoData(string title)
        {
            Initialize(title, "", false, false, 0);
        }

        public NotifyToDoData(string title, string content)
        {
            Initialize(title, content, false, false, 0);
        }

        public NotifyToDoData(string title, string content, bool executeState, bool checkState, int executeValue)
        {
            Initialize(title, content, executeState, checkState, executeValue);
        }

        private void Initialize(string title, string content, bool executeState, bool checkState, int executeValue)
        {
            Title = title;
            Content = content;
            ExecuteIconPath = "/MyLancher;component/Pics/stop.bmp";
            CheckIconPath = "/MyLancher;component/Pics/unchecked.bmp";
            ExecuteState = executeState;
            CheckState = checkState;
            ExecuteValue = executeValue;
        }

        private void UpdateExecuteIconImage()
        {
            if (ExecuteState)
            {
                ExecuteIconPath = "/MyLancher;component/Pics/execute.bmp";
                return;
            }
            ExecuteIconPath = "/MyLancher;component/Pics/stop.bmp";
        }

        private void UpdateCheckIconImage()
        {
            if (CheckState)
            {
                CheckIconPath = "/MyLancher;component/Pics/checked.bmp";
                return;
            }
            CheckIconPath = "/MyLancher;component/Pics/unchecked.bmp";
        }

        private void UpdateByExecuteState()
        {
            UpdateExecuteIconImage();
        }

        private void UpdateByCheckState()
        {
            UpdateCheckIconImage();
            if (CheckState)
            {
                ExecuteState = false;
                ExecuteValue = 100;
                ExecuteButtonVisibility = Visibility.Hidden;
                ExecuteValueVisibility = Visibility.Collapsed;
                return;
            }
            ExecuteButtonVisibility = Visibility.Visible;
            ExecuteValueVisibility = Visibility.Visible;
        }

        // Their methods defined by INotifyPropertyChanged. 
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
