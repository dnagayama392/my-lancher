﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel; /* ObservableCollection */

namespace MyLancher.Model
{
    /* 時刻を格納するクラス */
    public class DateTimeManager
    {
        private const int YEAR_INTERVALS =  5; // 5年先まで予約できます
        private const int MIN_MONTH      =  1;
        private const int MAX_MONTH      = 12;
        private const int MIN_DAY        =  1;
        private const int MIN_HOUR       =  0;
        private const int MAX_HOUR       = 23;
        private const int MIN_MINUTE     =  0;
        private const int MAX_MINUTE     = 59;
        private const int MIN_SECOND     =  0;
        private const int MAX_SECOND     = 59;

        private DateTime thisTime;
        private int year;
        private int month;
        private int day;
        private int hour;
        private int minute;
        private int second;

        private DateTime ThisTime
        {
            get
            {
                return thisTime;
            }
            set
            {
                if (thisTime != value)
                {
                    thisTime = value;
                }
            }
        }

        private int Year
        {
            get
            {
                return year;
            }
            set
            {
                if (year == value)
                {
                    return;
                }
                year = value;
            }
        }

        public int Month
        {
            get
            {
                return month;
            }
            set
            {
                if (month == value)
                {
                    return;
                }
                month = value;
            }
        }

        public int Day
        {
            get
            {
                return day;
            }
            set
            {
                if (day == value)
                {
                    return;
                }
                day = value;
            }
        }

        public int Hour
        {
            get
            {
                return hour;
            }
            set
            {
                if (hour == value)
                {
                    return;
                }
                if (value < MIN_HOUR || value > MAX_HOUR)
                {
                    return;
                }
                hour = value;
            }
        }

        public int Minute
        {
            get
            {
                return minute;
            }
            set
            {
                if (minute == value)
                {
                    return;
                }
                if (value < MIN_MINUTE || value > MAX_MINUTE)
                {
                    return;
                }
                minute = value;
            }
        }

        private int Second
        {
            get
            {
                return second;
            }
            set
            {
                if (second == value)
                {
                    return;
                }
                if (value < MIN_SECOND || value > MAX_SECOND)
                {
                    return;
                }
                second = value;
            }
        }

        public DateTimeManager()
        {
            Initialize();
        }

        public DateTimeManager(int year, int month, int day, int hour, int minute, int second)
        {
            SetDateTime(year, month, day, hour, minute, second);
        }

        public ObservableCollection<string> GetDayList()
        {
            return GetStrNumList(MIN_DAY, GetMaxDay());
        }

        public ObservableCollection<string> GetHourList()
        {
            return GetStrNumList(MIN_HOUR, MAX_HOUR);
        }

        public ObservableCollection<string> GetMinuteList()
        {
            return GetStrNumList(MIN_MINUTE, MAX_MINUTE, true);
        }

        public ObservableCollection<string> GetMonthList()
        {
            return GetStrNumList(MIN_MONTH, MAX_MONTH);
        }

        public ObservableCollection<string> GetSecondList()
        {
            return GetStrNumList(MIN_SECOND, MAX_SECOND, true);
        }

        public ObservableCollection<string> GetYearList()
        {
            var firstYear = Year;
            var lastYear = Year + YEAR_INTERVALS;
            return GetStrNumList(firstYear, lastYear);
        }

        public void SetDateTime(int year, int month, int day, int hour, int minute, int second)
        {
            ThisTime = new DateTime(year, month, day, hour, minute, second);
            Year   = ThisTime.Year;
            Month  = ThisTime.Month;
            Day    = ThisTime.Day;
            Hour   = ThisTime.Hour;
            Minute = ThisTime.Minute;
            Second = ThisTime.Second;
            SetCorrectDay();
        }

        public void SetNow()
        {
            ThisTime = DateTime.Now;
        }

        public string ToStrFullTime()
        {
            return ThisTime.ToString("yyyy年MM月dd日(ddd)  HH:mm:ss");
        }

        public long ToTimeValue()
        {
            var strValure = ThisTime.ToString("yyyyMMddHHmmss");
            var value = long.Parse(strValure);
            return value;
        }

        private void Initialize()
        {
            SetNow();
            Year   = ThisTime.Year;
            Month  = ThisTime.Month;
            Day    = ThisTime.Day;
            Hour   = ThisTime.Hour;
            Minute = ThisTime.Minute;
            Second = ThisTime.Second;
            SetCorrectDay();
        }

        private void SetCorrectDay()
        {
            var maxDay = GetMaxDay();
            if (Day > maxDay)
            {
                Day = maxDay;
            }
        }

        /// <summary>
        /// 範囲[min,max]の数をObservableCollection<string>型で返します。
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        private ObservableCollection<string> GetStrNumList(int min, int max)
        {
            return GetStrNumList(min, max, false);
        }

        private ObservableCollection<string> GetStrNumList(int min, int max, bool isZero)
        {
            if (min > max)
            {
                return null;
            }
            var strNumList = new ObservableCollection<string>();
            for (var addNum = min; addNum <= max; addNum++)
            {
                var strAddNum = ConvIntToStr2(addNum, isZero);
                strNumList.Add(strAddNum);
            }
            return strNumList;
        }

        /// <summary>
        /// int型引数を2文字以上のstring型にして返します
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        private string ConvIntToStr2(int num)
        {
            return ConvIntToStr2(num, false);
        }

        private string ConvIntToStr2(int num, bool isZero)
        {
            if (num >= 10)
            {
                return num.ToString();
            }
            if (isZero)
            {
                return @"0" + num.ToString();
            }
            return num.ToString();
        }

        private int GetMaxDay()
        {
            // DateTime.DaysInMonth を利用
            try
            {
                var maxDay = DateTime.DaysInMonth(Year, Month);
                return maxDay;
            }
            catch(System.ArgumentException)
            {
                return 0;
            }
        }
    }
}
