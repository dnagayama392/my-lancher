﻿using System;
using System.Collections;
using System.Collections.ObjectModel; /* Collection */

namespace MyLancher.Model
{
    /// <summary>
    /// string要素を持つObservableCollectionのリスト
    /// </summary>
    public class StringList
    {
        private Collection<string> strList;

        private Collection<string> StrList
        {
            get
            {
                return strList;
            }
            set
            {
                strList = value;
            }
        }

        public int Count
        {
            get
            {
                return StrList.Count;
            }
        }

        public string this[int index]
        {
            get
            {
                return StrList[index];
            }
            set
            {
                StrList[index] = value;
            }
        }

        public StringList()
        {
            Initialize();
        }

        public StringList(string[] strary)
        {
            StrList = new Collection<string>(strary);
        }

        public void Add(string str)
        {
            StrList.Add(str);
        }

        public bool Contains(string str)
        {
            return StrList.Contains(str);
        }

        public int IndexOf(string str)
        {
            return StrList.IndexOf(str);
        }


        public Collection<string> GetList()
        {
            return StrList;
        }

        private void Initialize()
        {
            StrList = new Collection<string>();
        }
    }
}
