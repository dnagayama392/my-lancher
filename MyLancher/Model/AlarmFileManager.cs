﻿/* AlarmFileManager.cs */
using System.Collections.ObjectModel; /* ObservableCollection */
using System.IO; /* File */
using System.Linq;
using System;
using MyLancher.ViewModel; /* AlarmListViewModel */

namespace MyLancher.Model
{
    /// <summary>
    /// ファイル Alarm.csv への入出力を管理
    /// </summary>
    public class AlarmFileManager
    {
        private const string ALARM_FILE_NAME = @"alarm.csv";
        private const string ALARM_FILE_PATH
            = DataDirectoryManager.DATA_DIRECTORY_PATH
            + DataDirectoryManager.PATH_SEPARATE + ALARM_FILE_NAME;
        private const string ALARM_FILE_HEADER
            = "#Validation,NextTime,AlarmWay,AlarmTimes,AlarmInterbal,FirstTime,LastTime,x\r\n";

        private Collection<NotifyAlarmData> alarmDataCollection;

        public Collection<NotifyAlarmData> AlarmDataCollection
        {
            get
            {
                return alarmDataCollection;
            }
            set
            {
                if (alarmDataCollection != value)
                {
                    alarmDataCollection = value;
                }
            }
        }

        public AlarmFileManager()
        {
            Initialize();
        }

        public void Write(Collection<AlarmListViewModel> alarmDataCollection)
        {
            if (alarmDataCollection.Count == 0)
            {
                return;
            }
            var fileContent = new System.Text.StringBuilder();
            //string fileContent = "";
            fileContent.Append(ALARM_FILE_HEADER);
            foreach (var alarmData in alarmDataCollection)
            {
                fileContent.Append(alarmData.InnerValidation + ",");
                fileContent.Append(alarmData.InnerNextTime + ",");
                fileContent.Append(alarmData.InnerAlarmWay + ",");
                fileContent.Append(alarmData.InnerAlarmTimes + ",");
                fileContent.Append(alarmData.InnerAlarmInterbal + ",");
                fileContent.Append(alarmData.InnerFirstTime + ",");
                fileContent.Append(alarmData.InnerLastTime + ",");
                fileContent.Append("x\r\n");
            }
            File.WriteAllText(ALARM_FILE_PATH, fileContent.ToString());
        }

        public Collection<AlarmListViewModel> Read()
        {
            var tmp = new System.Collections.Generic.List<AlarmListViewModel>();

            var alarmDataCollection = new Collection<AlarmListViewModel>();
            var allLines = File.ReadAllLines(ALARM_FILE_PATH);
            var isFirst = new RichBool(true);
            foreach (var lineData in allLines)
            {
                var alarmData = ConvLineToAlarmData(lineData, isFirst);
                if (alarmData != null)
                {
                    tmp.Add(alarmData);
//                    alarmDataCollection.Add(alarmData);
                }
            }
            foreach (var item in tmp.OrderBy(x => x.GetNextTimeValue()))
            {
                alarmDataCollection.Add(item);
            }
            return alarmDataCollection;
        }

        private void Initialize()
        {
            if (!File.Exists(ALARM_FILE_PATH))
            {
                CreateAlarmFile();
            }
        }

        private void CreateAlarmFile()
        {
            string fileContent = "";
            fileContent += ALARM_FILE_HEADER;
            File.AppendAllText(ALARM_FILE_PATH, fileContent);
        }

        private AlarmListViewModel ConvLineToAlarmData(string linedata, RichBool isFirst)
        {
            if (isFirst.Bool)
            {
                isFirst.False();
                return null;
            }
            var alarmData = new AlarmListViewModel();
            var alarmDataPack = linedata.Split(',');
            if(alarmDataPack.Length != ALARM_FILE_HEADER.Split(',').Length)
            {
                return null;
            }
            alarmData.InnerValidation = alarmDataPack[0];
            alarmData.InnerNextTime = alarmDataPack[1];
            alarmData.InnerAlarmWay = alarmDataPack[2];
            alarmData.InnerAlarmTimes = alarmDataPack[3];
            alarmData.InnerAlarmInterbal = alarmDataPack[4];
            alarmData.InnerFirstTime = alarmDataPack[5];
            alarmData.InnerLastTime = alarmDataPack[6];
            return alarmData;
        }

        public int Element()
        {
            var headerAry = ALARM_FILE_HEADER.Split(',');
            return headerAry.Length;
        }
    }
}
