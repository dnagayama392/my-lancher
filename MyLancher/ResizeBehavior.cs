﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace MyLancher
{
    public class ResizeBehavior : Behavior<CheckBox>
    {
        public ResizeBehavior()
        {
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.Unchecked += ResizeHeightOfMainWindow;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.Unchecked -= ResizeHeightOfMainWindow;
        }

        private void ResizeHeightOfMainWindow(object sender, EventArgs e)
        {
            if (!(bool)AssociatedObject.IsChecked)
            {
                //ViewModel.MainViewModel main = new ViewModel.MainViewModel();
                //main.WindowHeight = 80;
            }
        }
    }
}
