﻿/* AlarmWindow.xaml.cs */
using System.Windows; /* Window */
using System.Windows.Controls.Primitives; /* DragDeltaEventArgs */
using GalaSoft.MvvmLight.Messaging; /* Messenger, NotificationMessage */

namespace MyLancher
{
    /// <summary>
    /// AlarmWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class AlarmWindow : Window
    {
        public AlarmWindow()
        {
            Messenger.Default.Register<NotificationMessage>(this, "closeAlarm", msg =>
            {
                this.Close();
            });

            InitializeComponent();
        }

        private void WindowBackground_DragDelta(object sender, DragDeltaEventArgs e)
        {
            Left += e.HorizontalChange;
            Top += e.VerticalChange;
        }
    }
}
