﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
using System.Windows;
using System.Windows.Controls;
//using System.Windows.Data;
//using System.Windows.Documents;
//using System.Windows.Input;
//using System.Windows.Media;
//using System.Windows.Media.Imaging;
//using System.Windows.Shapes;
using System.Windows.Controls.Primitives; /* DragDeltaEventArgs */
using System.Collections.ObjectModel; /* ObservableCollection */
using MyLancher.ViewModel; /* MatchingPatternViewModel */
using GalaSoft.MvvmLight.Messaging; /* Messenger */

namespace MyLancher
{
    /// <summary>
    /// SettingWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class SettingWindow : Window
    {
        public SettingWindow()
        {
            Messenger.Default.Register<NotificationMessage>(this, "close", msg =>
            {
                this.Close();
            });
            InitializeComponent();
        }

        private void WindowBackground_DragDelta(object sender, DragDeltaEventArgs e)
        {
            Left += e.HorizontalChange;
            Top += e.VerticalChange;
        }

        private void ToClose(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
