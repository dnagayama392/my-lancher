﻿/* ToDoWindow.xaml.cs */
using System.Windows; /* Window */
using System.Windows.Controls.Primitives; /* DragDeltaEventArgs */
using GalaSoft.MvvmLight.Messaging; /* Messenger */

namespace MyLancher
{
    /// <summary>
    /// ToDoWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class ToDoWindow : Window
    {
        public ToDoWindow()
        {
            Messenger.Default.Register<NotificationMessage>(this, "closeToDo", msg =>
            {
                this.Close();
            });
            InitializeComponent();
        }

        //ウィンドウのドラッグでウィンドウ移動
        private void WindowBackground_DragDelta(object sender, DragDeltaEventArgs e)
        {
            Left += e.HorizontalChange;
            Top += e.VerticalChange;
        }
    }
}
