﻿/* MainWindow.xaml.cs */
using System.Windows; /* Window */
using System.Windows.Controls.Primitives; /* DragDeltaEventArgs */
using System.Windows.Input; /* ModifierKeys, KeyInterop, Key */
using System; /* IntPtr, EventArgs */
using System.Windows.Interop; /* WindowInteropHelper, ComponentDispatcher, MSG */
using System.Runtime.InteropServices; /* DllImport */
using System.Windows.Forms; /* NotifyIcon */
using GalaSoft.MvvmLight.Messaging; /* Messenger, NotificationMessage */

namespace MyLancher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private NotifyIcon _notifyIcon;
        // HotKey登録時に指定するID
        // 0x0000～0xbfff で指定すること
        // 0xc000～0xffff はDLL用なので使用不可能
        private const int HOTKEY_ID1 = 0x0001;
        private const int HOTKEY_ID2 = 0x0002;
        // HotKey Message ID
        private const int WM_HOTKEY = 0x0312;
        private IntPtr _windowHandle;

        // 戻り値：成功 = 0以外、失敗 = 0（既に他が登録済み)
        [DllImport("user32.dll")]
        private static extern int RegisterHotKey(IntPtr hWnd, int id, int MOD_KEY, int VK);

        // 戻り値：成功 = 0以外、失敗 = 0
        [DllImport("user32.dll")]
        private static extern int UnregisterHotKey(IntPtr hWnd, int id);
        
        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            Messenger.Default.Register<NotificationMessage>(this, "invisibleLauncher", msg =>
            {
                Passive();
//                this.Close();
            });

            InitializeComponent();

            //タスクトレイアイコンを初期化する
            _notifyIcon = new NotifyIcon();
            _notifyIcon.Text = "My Launcher+";
            // DONE: 埋め込みに変更
            var comPic = new MyLancher.Model.CommonsPict();
            _notifyIcon.Icon = comPic.GetWorldIcon();

            //タスクトレイに表示する
            _notifyIcon.Visible = true;

            //アイコンにコンテキストメニュー「終了」を追加する
            ContextMenuStrip menuStrip = new ContextMenuStrip();

            ToolStripMenuItem exitItem = new ToolStripMenuItem();
            exitItem.Text = "終了";
            menuStrip.Items.Add(exitItem);
            exitItem.Click += new EventHandler(exitItem_Click);

            _notifyIcon.ContextMenuStrip = menuStrip;

            //タスクトレイアイコンのクリックイベントハンドラを登録する
            _notifyIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(_notifyIcon_MouseClick);

            // WindowのHandleを取得
            var _host = new WindowInteropHelper(this);
            this._windowHandle = _host.Handle;

            // HotKeyを設定
            SetupHotKey();
            ComponentDispatcher.ThreadPreprocessMessage
                += ComponentDispatcher_ThreadPreprocessMessage;
        }

        // ウィンドウが閉じられる前に発生するイベントのハンドラ
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                //閉じるのをキャンセルする
                e.Cancel = true;

                //ウィンドウを非可視にする
                Visibility = System.Windows.Visibility.Collapsed;
            }
            catch { }
        }

        // NotifyIconのクリックイベントのハンドラ
        private void _notifyIcon_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Left)
                {
                    //ウィンドウを可視化
                    Active();
                }
            }
            catch { }
        }

        // 終了メニューのイベントハンドラ
        private void exitItem_Click(object sender, EventArgs e)
        {
            try
            {
                _notifyIcon.Dispose();
                System.Windows.Application.Current.Shutdown();
            }
            catch { }
        }

        /// <summary>
        /// 背景部分をドラッグして移動可能に
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WindowBackground_DragDelta(object sender, DragDeltaEventArgs e)
        {
            Left += e.HorizontalChange;
            Top += e.VerticalChange;
        }

        // HotKeyの登録
        private void SetupHotKey()
        {
             RegisterHotKey(this._windowHandle, HOTKEY_ID1, (int)ModifierKeys.Alt, 
                KeyInterop.VirtualKeyFromKey(Key.V));

             RegisterHotKey(this._windowHandle, HOTKEY_ID2, (int)ModifierKeys.Alt,
                KeyInterop.VirtualKeyFromKey(Key.Down));
        }

        // HotKeyの登録削除
        private void Window_Closed(object sender, EventArgs e)
        {
            UnregisterHotKey(this._windowHandle, HOTKEY_ID1);
            UnregisterHotKey(this._windowHandle, HOTKEY_ID2);
        }

        // HotKeyの動作を設定する
        public void ComponentDispatcher_ThreadPreprocessMessage(ref MSG msg, ref bool handled)
        {
            if (msg.message == WM_HOTKEY)
            {
                if (msg.wParam.ToInt32() == HOTKEY_ID1)
                {
                    PushHotKey();
                }
                else if (msg.wParam.ToInt32() == HOTKEY_ID2)
                {
                    PushHotKey();
                }
            }
        }

        private void PushHotKey()
        {
            if (Visibility == System.Windows.Visibility.Visible)
            {
                Passive();
                return;
            }
            Active();
        }

        private void Active()
        {
            Visibility = System.Windows.Visibility.Visible;
            WindowState = System.Windows.WindowState.Normal;
            this.Activate();
        }

        private void Passive()
        {
            Window_Closing(new object(), new System.ComponentModel.CancelEventArgs());
        }
    }
}