﻿/* AlarmViewModel.cs */

using System; /* DateTime */
using System.Collections.ObjectModel; /* ObservableCollection */
using System.Windows.Media.Imaging; /* BitmapImage */
using System.Linq;
using System.Windows; /* Visibility */
using System.Windows.Threading; /* DispatcherTimer */
using GalaSoft.MvvmLight; /* ViewModelBase */
using GalaSoft.MvvmLight.Command; /* RelayCommand */
using GalaSoft.MvvmLight.Messaging; /* Messenger */
using MyLancher.Model; /* AlarmModelManager */

namespace MyLancher.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class AlarmViewModel : ViewModelBase
    {
        private AlarmModelManager alarmManager;
        /* Top */
        private string currentTime;
        private string nextTime;
        private long nextTimeValue;
        private bool additionalState;
        /* Addition Field */
        private Visibility additionFieldVisibility;
        private int selectedTabIndex;
        private DateTimeManager handledTime;
        private ObservableCollection<string> yearList;
        private int selectedYearIndex;
        private ObservableCollection<string> monthList;
        private int selectedMonthIndex;
        private ObservableCollection<string> dayList;
        private int selectedDayIndex;
        private ObservableCollection<string> hourList;
        private int selectedHourIndex;
        private ObservableCollection<string> minuteList;
        private int selectedMinuteIndex;
        private ObservableCollection<string> secondList;
        private int selectedSecondIndex;
        private RelayCommand addAlarm;
        private bool repetitionalState;
        /* Repetition Field */
        private Visibility repetitionFieldVisibility;
        /* Bottom */
        private bool listVisibleState;
        //private RelayCommand thisClose;
        private RelayCommand closeApply;
        private RelayCommand closeRelaod;
        /* List Field */
        private Visibility listFieldVisibility;
        private ObservableCollection<AlarmListViewModel> alarmList;
        private AlarmListViewModel selectedAlarm;
        private int selectedAlarmIndex;
        private RelayCommand deleteAlarm;
        private RelayCommand invisibleApply;
        private RelayCommand invisibleReload;
        private RelayCommand applyAlarm;
        /* Window Layout */
        private int windowHeight;
        private int windowWidth;
        private int mainFieldHeight;
        private int mainFieldWidth;
        private int listFieldWidth;
        private BitmapImage alarmIcon;

        private AlarmModelManager AlarmManager
        {
            get
            {
                return alarmManager;
            }
            set
            {
                if (alarmManager != value)
                {
                    alarmManager = value;
                }
            }
        }

        public string CurrentTime
        {
            get
            {
                return currentTime;
            }
            set
            {
                if (currentTime != value)
                {
                    currentTime = value;
                    RaisePropertyChanged("CurrentTime");
                }
            }
        }

        public string NextTime
        {
            get
            {
                return nextTime;
            }
            set
            {
                if (nextTime != value)
                {
                    nextTime = value;
                    RaisePropertyChanged("NextTime");
                }
            }
        }

        public long NextTimeValue
        {
            get
            {
                return nextTimeValue;
            }
            set
            {
                if (nextTimeValue != value)
                {
                    nextTimeValue = value;
                    RaisePropertyChanged("NextTimeValue");
                }
            }
        }

        public bool AdditionalState
        {
            get
            {
                return additionalState;
            }
            set
            {
                if (additionalState != value)
                {
                    additionalState = value;
                    InitializeAdditionalTime(value);
                    AdditionFieldVisibility = ToVisibility(value);
                    SetWindowLayout();
                }
                //RaisePropertyChanged("AdditionalState");
            }
        }

        public Visibility AdditionFieldVisibility
        {
            get
            {
                return additionFieldVisibility;
            }
            set
            {
                if (additionFieldVisibility != value)
                {
                    additionFieldVisibility = value;
                }
                RaisePropertyChanged("AdditionFieldVisibility");
            }
        }

        public int SelectedTabIndex
        {
            get
            {
                return selectedTabIndex;
            }
            set
            {
                if (selectedTabIndex != value)
                {
                    selectedTabIndex = value;
                }
            }
        }

        public DateTimeManager HandledTime
        {
            get
            {
                return handledTime;
            }
            set
            {
                if (handledTime != value)
                {
                    handledTime = value;
                }
            }
        }

        public ObservableCollection<string> YearList
        {
            get
            {
                return yearList;
            }
            set
            {
                if (yearList != value)
                {
                    yearList = value;
                }
            }
        }

        public int SelectedYearIndex
        {
            get
            {
                return selectedYearIndex;
            }
            set
            {
                if (selectedYearIndex != value)
                {
                    selectedYearIndex = value;
                    if (SelectedMonthIndex == 1)
                    {

                    }
                }
                RaisePropertyChanged("SelectedYearIndex");
            }
        }

        public ObservableCollection<string> MonthList
        {
            get
            {
                return monthList;
            }
            set
            {
                if (monthList != value)
                {
                    monthList = value;
                }
            }
        }

        public int SelectedMonthIndex
        {
            get
            {
                return selectedMonthIndex;
            }
            set
            {
                if (selectedMonthIndex != value)
                {
                    selectedMonthIndex = value;
                }
                RaisePropertyChanged("SelectedMonthIndex");
            }
        }

        public ObservableCollection<string> DayList
        {
            get
            {
                return dayList;
            }
            set
            {
                if (dayList != value)
                {
                    dayList = value;
                }
            }
        }

        public int SelectedDayIndex
        {
            get
            {
                return selectedDayIndex;
            }
            set
            {
                if (selectedDayIndex != value)
                {
                    selectedDayIndex = value;
                }
                RaisePropertyChanged("SelectedDayIndex");
            }
        }

        public ObservableCollection<string> HourList
        {
            get
            {
                return hourList;
            }
            set
            {
                if (hourList != value)
                {
                    hourList = value;
                }
            }
        }

        public int SelectedHourIndex
        {
            get
            {
                return selectedHourIndex;
            }
            set
            {
                if (selectedHourIndex != value)
                {
                    selectedHourIndex = value;
                }
                RaisePropertyChanged("SelectedHourIndex");
            }
        }

        public ObservableCollection<string> MinuteList
        {
            get
            {
                return minuteList;
            }
            set
            {
                if (minuteList != value)
                {
                    minuteList = value;
                }
            }
        }

        public int SelectedMinuteIndex
        {
            get
            {
                return selectedMinuteIndex;
            }
            set
            {
                if (selectedMinuteIndex != value)
                {
                    selectedMinuteIndex = value;
                }
                RaisePropertyChanged("SelectedMinuteIndex");
            }
        }

        public ObservableCollection<string> SecondList
        {
            get
            {
                return secondList;
            }
            set
            {
                if (secondList != value)
                {
                    secondList = value;
                }
            }
        }

        public int SelectedSecondIndex
        {
            get
            {
                return selectedSecondIndex;
            }
            set
            {
                if (selectedSecondIndex != value)
                {
                    selectedSecondIndex = value;
                }
                RaisePropertyChanged("SelectedSecondIndex");
            }
        }

        public bool RepetitionalState
        {
            get
            {
                return repetitionalState;
            }
            set
            {
                if (repetitionalState != value)
                {
                    repetitionalState = value;
                    RepetitionFieldVisibility = ToVisibility(value);
                    SetWindowLayout();
                }
            }
        }

        public Visibility RepetitionFieldVisibility
        {
            get
            {
                return repetitionFieldVisibility;
            }
            set
            {
                if (repetitionFieldVisibility != value)
                {
                    repetitionFieldVisibility = value;
                }
                RaisePropertyChanged("RepetitionFieldVisibility");
            }
        }

        public RelayCommand AddAlarm
        {
            get
            {
                if (addAlarm == null)
                {
                    addAlarm = new RelayCommand(() =>
                    {
                        AddAlarmTime();
                    });
                }
                return addAlarm;
            }
        }

        public bool ListVisibleState
        {
            get
            {
                return listVisibleState;
            }
            set
            {
                if (listVisibleState != value)
                {
                    listVisibleState = value;
                    RaisePropertyChanged("ListVisibleState");
                    ListFieldVisibility = ToVisibility(value);
                    SetWindowLayout();
                }
            }
        }

        public RelayCommand CloseApply
        {
            get
            {
                if (closeApply == null)
                {
                    closeApply = new RelayCommand(() =>
                    {
                        CloseWindow();
                        ApplyAlarmList();
                    });
                }
                return closeApply;
            }
        }

        public RelayCommand CloseRelaod
        {
            get
            {
                if (closeRelaod == null)
                {
                    closeRelaod = new RelayCommand(() =>
                    {
                        CloseWindow();
                        RelaodAlarmList();
                    });
                }
                return closeRelaod;
            }
        }

        /* List Field */
        public Visibility ListFieldVisibility
        {
            get
            {
                return listFieldVisibility;
            }
            set
            {
                if (listFieldVisibility != value)
                {
                    listFieldVisibility = value;
                }
                RaisePropertyChanged("ListFieldVisibility");
            }
        }

        public ObservableCollection<AlarmListViewModel> AlarmList
        {
            get
            {
                return alarmList;
            }
            set
            {
                if (alarmList != value)
                {
                    alarmList = value;
                }
            }
        }

        public AlarmListViewModel SelectedAlarm
        {
            get
            {
                return selectedAlarm;
            }
            set
            {
                if (selectedAlarm != value)
                {
                    selectedAlarm = value;
                    RaisePropertyChanged("SelectedAlarm");
                }
            }
        }

        public int SelectedAlarmIndex
        {
            get
            {
                return selectedAlarmIndex;
            }
            set
            {
                if (selectedAlarmIndex != value)
                {
                    selectedAlarmIndex = value;
                    RaisePropertyChanged("SelectedAlarmIndex");
                }
            }
        }

        public RelayCommand DeleteAlarm
        {
            get
            {
                if (deleteAlarm == null)
                {
                    deleteAlarm = new RelayCommand(() =>
                    {
                        if (AlarmList.Count == 0)
                        {
                            return;
                        }
                        //SelectedAlarm = AlarmList[SelectedAlarmIndex];
                        System.Diagnostics.Debug.WriteLine(SelectedAlarm.FirstTime);
                        var index = AlarmList.IndexOf(SelectedAlarm);
                        AlarmList.Remove(SelectedAlarm);
                        if (index == AlarmList.Count)
                        {
                            index--;
                        }
                        if (index < 0)
                        {
                            SelectedAlarm = null;
                            return;
                        }
                        SelectedAlarm = AlarmList[index];
                    });
                }
                return deleteAlarm;
            }
        }

        public RelayCommand InvisibleApply
        {
            get
            {
                if (invisibleApply == null)
                {
                    invisibleApply = new RelayCommand(() =>
                    {
                        ListVisibleState = false;
                        ApplyAlarmList();
                    });
                }
                return invisibleApply;
            }
        }

        public RelayCommand InvisibleReload
        {
            get
            {
                if (invisibleReload == null)
                {
                    invisibleReload = new RelayCommand(() =>
                    {
                        ListVisibleState = false;
                        RelaodAlarmList();
                    });
                }
                return invisibleReload;
            }
        }

        public RelayCommand ApplyAlarm
        {
            get
            {
                if (applyAlarm == null)
                {
                    applyAlarm = new RelayCommand(() =>
                    {
                        ApplyAlarmList();
                    });
                }
                return applyAlarm;
            }
        }

        /* Window Layout */
        public int WindowHeight
        {
            get
            {
                return windowHeight;
            }
            set
            {
                if (windowHeight != value)
                {
                    windowHeight = value;
                    RaisePropertyChanged("WindowHeight");
                }
            }
        }

        public int WindowWidth
        {
            get
            {
                return windowWidth;
            }
            set
            {
                if (windowWidth != value)
                {
                    windowWidth = value;
                    RaisePropertyChanged("WindowWidth");
                }
            }
        }

        public int MainFieldHeight
        {
            get
            {
                return mainFieldHeight;
            }
            set
            {
                if (mainFieldHeight != value)
                {
                    mainFieldHeight = value;
                    RaisePropertyChanged("MainFieldHeight");
                }
            }
        }

        public int MainFieldWidth
        {
            get
            {
                return mainFieldWidth;
            }
            set
            {
                if (mainFieldWidth != value)
                {
                    mainFieldWidth = value;
                    RaisePropertyChanged("MainFieldWidth");
                }
            }
        }

        public int ListFieldWidth
        {
            get
            {
                return listFieldWidth;
            }
            set
            {
                if (listFieldWidth != value)
                {
                    listFieldWidth = value;
                    RaisePropertyChanged("ListFieldWidth");
                }
            }
        }

        public BitmapImage AlarmIcon
        {
            get
            {
                return alarmIcon;
            }
            set
            {
                if (alarmIcon != value)
                {
                    alarmIcon = value;
                    RaisePropertyChanged("AlarmIcon");
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the AlarmViewModel class.
        /// </summary>
        public AlarmViewModel()
        {
            Initialize();
        }

        private void AddAlarmTime()
        {
            // 以下は経過時間
            // 0.0
            var strFirstTime = ToSSString(
                YearList[SelectedYearIndex],     MonthList[SelectedMonthIndex],
                DayList[SelectedDayIndex],       HourList[SelectedHourIndex],
                MinuteList[SelectedMinuteIndex], SecondList[SelectedSecondIndex]
                );
            // 0.22
            var additionAlarm = new AlarmListViewModel(strFirstTime);
            var nowsss = DateTime.Now.ToString("yyyy/MM/dd/HH/mm/ss");
            var nowAlarm = new AlarmListViewModel(nowsss);
            if (additionAlarm.GetNextTimeValue() < nowAlarm.GetNextTimeValue())
            {
                System.Windows.MessageBox.Show("現在以降の時刻を入力してください");
                return;
            }
            // 0.0
            AlarmManager.AddAlarm(additionAlarm);
            // 0.0
            var index = AlarmList.Where(x => x.GetNextTimeValue() < additionAlarm.GetNextTimeValue()).Count();
            // 0.0
            AlarmList.Insert(index, additionAlarm);
            // 0.0
            SetNextTime();
        }

        private string ToSSString(string year, string month, string day, string hour, string minute, string second)
        {
            var ssstring = "";
            ssstring += year + "/" + month  + "/" + day + "/";
            ssstring += hour + "/" + minute + "/" + second;
            return ssstring;
        }

        private void Initialize()
        {
            var comPic = new CommonsPict();
            AlarmIcon = comPic.ToBitmapImage(comPic.GetClockIcon());
            AlarmManager = new AlarmModelManager();
            HandledTime = new DateTimeManager();
            HandledTime.SetNow();
            InitializeMainBar();
            SetWindowLayout();
        }

        private void InitializeMainBar()
        {
            CurrentTime = AlarmManager.GetStrCurrentTime();
            AdditionalState = true;
            InitializeAdditionField();
            ListVisibleState = true;
            ListFieldVisibility = ToVisibility(ListVisibleState);
            InitializeAlarmListField();
            SetNextTime();
            var timer = new DispatcherTimer();
            timer.Tick += timer_tick;
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Start();
        }

        private void SetNextTime()
        {
            NextTime = "次回通知時刻\r\n   " + AlarmList[0].NextTime;
            NextTimeValue = AlarmList[0].GetNextTimeValue();
        }

        private void InitializeAdditionField()
        {
            AdditionFieldVisibility = ToVisibility(AdditionalState);
            SelectedTabIndex = 0;
            RepetitionalState = false;
            InitializeRepetitionFieald();
            InitializeAdditionalTime();
        }

        private void InitializeRepetitionFieald()
        {
            RepetitionFieldVisibility = ToVisibility(RepetitionalState);
        }

        private void InitializeAdditionalTime()
        {
            InitializeAdditionalTime(true);
        }

        private void InitializeAdditionalTime(bool additionalState)
        {
            if (!additionalState)
            {
                return;
            }
            HandledTime.SetNow();
            YearList            = HandledTime.GetYearList();
            SelectedYearIndex   = 0;
            MonthList           = HandledTime.GetMonthList();
            SelectedMonthIndex  = HandledTime.Month - 1;
            DayList             = HandledTime.GetDayList();
            SelectedDayIndex    = HandledTime.Day - 1;
            HourList            = HandledTime.GetHourList();
            SelectedHourIndex   = HandledTime.Hour;
            MinuteList          = HandledTime.GetMinuteList();
            SelectedMinuteIndex = HandledTime.Minute;
            SecondList          = HandledTime.GetSecondList();
            SelectedSecondIndex = 0;
        }

        private void InitializeAlarmListField()
        {
            //if (!ListVisibleState)
            //{
            //    AlarmList = new ObservableCollection<NotifyAlarmData>();
            //    return;
            //}
            AlarmList = new ObservableCollection<AlarmListViewModel>(AlarmManager.AlarmDataCollection);
            if (AlarmList.Count > 0)
            {
                SelectedAlarm = AlarmList[0];
            }
        }

        private void SetWindowLayout()
        {
            var addAlarmFieldHeight = 380 + 5 * 2;
            if (!RepetitionalState)
            {
                addAlarmFieldHeight -= 225;
            }
            if (!AdditionalState)
            {
                addAlarmFieldHeight = 0;
            }
            MainFieldHeight = 70 + 20 + addAlarmFieldHeight + 20 + 30;
            WindowHeight = 5 * 2 + MainFieldHeight + 5 * 2;
            SetWidth();
        }

        private void SetWidth()
        {
            MainFieldWidth = 240;
            ListFieldWidth = 0;
            if (ListVisibleState)
            {
                ListFieldWidth += 300;
                //ListFieldWidth += 600;
                WindowWidth = 5 * 2 + MainFieldWidth + 5 * 2 + ListFieldWidth + 5 * 2;
                return;
            }
            WindowWidth = 5 * 2 + MainFieldWidth + 5 * 2;
        }

        private void CloseWindow()
        {
            var message = new NotificationMessage(this, "message");
            Messenger.Default.Send(message, "closeAlarm");
        }

        private void RelaodAlarmList()
        {
            AlarmManager.ReadAlarm();
            InitializeAlarmListField();
        }

        private void ApplyAlarmList()
        {
            AlarmManager.AlarmDataCollection = new Collection<AlarmListViewModel>(AlarmList);
            AlarmManager.WriteAlarm();
        }

        /// <summary>
        /// 時刻更新に伴う処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_tick(object sender, EventArgs e)
        {
            AlarmManager.ResetTime();
            CurrentTime = AlarmManager.GetStrCurrentTime();
            if (AlarmManager.GetNowTimeValue() == NextTimeValue)
            {
                if (NextTimeToNext() != null)
                {
                    Alarm();
                }
            }
        }

        private AlarmListViewModel NextTimeToNext()
        {
            if (AlarmList.Count <= 0)
            {
                return null;
            }
            AlarmList.RemoveAt(0);
            if (AlarmList.Count == 0)
            {
                NextTime = "次回通知時刻\r\n   なし";
                NextTimeValue = 0;
                return null;
            }
            NextTime = "次回通知時刻\r\n   " + AlarmList[0].NextTime;
            NextTimeValue = AlarmList[0].GetNextTimeValue();
            return AlarmList[0];
        }

        /// <summary>
        /// アラーム通知
        /// </summary>
        private void Alarm()
        {
            System.Windows.MessageBox.Show("設定時刻になりました。","Alarm",MessageBoxButton.OK,MessageBoxImage.Information);
        }

        private Visibility ToVisibility(bool b)
        {
            if (b)
            {
                return Visibility.Visible;
            }
            return Visibility.Collapsed;
        }
    }
}