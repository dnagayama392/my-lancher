﻿using GalaSoft.MvvmLight;
using System; /* DateTime */
using MyLancher.Model; /* DateTimeManager */

namespace MyLancher.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class AlarmListViewModel : ViewModelBase
    {
        public const string VALID_TEXT   = "有効";
        public const string INVALID_TEXT = "無効";
        private const int NUM_DATE_TIME_ELEMENTS = 6;
        private const string NO_INTERBAL    = "None";
        private const string INNER_VAILID   = "Valid";
        private const string INNER_INVAILID = "Invalid";

        private string nextTime;
        private string firstTime;
        private string lastTime;
        private string alarmTimes;
        private string alarmInterbal;
        private string alarmWay;
        private string validation;
        private string innerNextTime;
        private string innerFirstTime;
        private string innerLastTime;
        private string innerAlarmTimes;
        private string innerAlarmInterbal;
        private string innerAlarmWay;
        private string innerValidation;

        public string NextTime
        {
            get
            {
                return nextTime;
            }
            set
            {
                if (nextTime != value)
                {
                    nextTime = value;
                }
            }
        }

        public string FirstTime
        {
            get
            {
                return firstTime;
            }
            set
            {
                if (firstTime != value)
                {
                    firstTime = value;
                }
            }
        }

        public string LastTime
        {
            get
            {
                return lastTime;
            }
            set
            {
                if (lastTime != value)
                {
                    lastTime = value;
                }
            }
        }

        public string AlarmTimes
        {
            get
            {
                return alarmTimes;
            }
            set
            {
                if (alarmTimes != value)
                {
                    alarmTimes = value;
                }
            }
        }

        public string AlarmInterbal
        {
            get
            {
                return alarmInterbal;
            }
            set
            {
                if (alarmInterbal != value)
                {
                    alarmInterbal = value;
                }
            }
        }

        public string AlarmWay
        {
            get
            {
                return alarmWay;
            }
            set
            {
                if (alarmWay != value)
                {
                    alarmWay = value;
                }
            }
        }

        public string Validation
        {
            get
            {
                return validation;
            }
            set
            {
                if (validation != value)
                {
                    validation = value;
                }
            }
        }

        public string InnerNextTime
        {
            get
            {
                return innerNextTime;
            }
            set
            {
                if (innerNextTime != value)
                {
                    innerNextTime = value;
                    nextTime = ConvertToViewTime(value);
                }
            }
        }

        public string InnerFirstTime
        {
            get
            {
                return innerFirstTime;
            }
            set
            {
                if (innerFirstTime != value)
                {
                    innerFirstTime = value;
                    firstTime = ConvertToViewTime(value);
                }
            }
        }

        public string InnerLastTime
        {
            get
            {
                return innerLastTime;
            }
            set
            {
                if (innerLastTime != value)
                {
                    innerLastTime = value;
                    lastTime = ConvertToViewTime(value);
                }
            }
        }

        public string InnerAlarmTimes
        {
            get
            {
                return innerAlarmTimes;
            }
            set
            {
                if (innerAlarmTimes != value)
                {
                    innerAlarmTimes = value;
                    alarmTimes = value;
                }
            }
        }

        public string InnerAlarmInterbal
        {
            get
            {
                return innerAlarmInterbal;
            }
            set
            {
                if (innerAlarmInterbal != value)
                {
                    innerAlarmInterbal = value;
                    alarmInterbal = value;
                }
            }
        }

        public string InnerAlarmWay
        {
            get
            {
                return innerAlarmWay;
            }
            set
            {
                if (innerAlarmWay != value)
                {
                    innerAlarmWay = value;
                    alarmWay = value;
                }
            }
        }

        public string InnerValidation
        {
            get
            {
                return innerValidation;
            }
            set
            {
                if (innerValidation != value)
                {
                    innerValidation = value;
                    validation = ConvertToViewValidation(value);
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the AlarmListViewModel class.
        /// </summary>
        public AlarmListViewModel()
        {
            Initialize();
        }

        public AlarmListViewModel(string firstTime)
        {
            InnerFirstTime = firstTime;
            InnerNextTime = firstTime;
            InnerAlarmInterbal = NO_INTERBAL;
            InnerAlarmTimes = "1";
            InnerLastTime = firstTime;
            InnerAlarmWay = "Baloon";
            InnerValidation = INNER_VAILID;
        }

        public long GetNextTimeValue()
        {
            var timePack = InnerNextTime.Split('/');
            if (!IsCorrectSintaxOfDateTime(timePack))
            {
                return 0;
            }
            var year   = long.Parse(timePack[0]);
            var month  = long.Parse(timePack[1]);
            var day    = long.Parse(timePack[2]);
            var hour   = long.Parse(timePack[3]);
            var minute = long.Parse(timePack[4]);
            var second = long.Parse(timePack[5]);
            var nextTimeValue = ((((year*100+month)*100+day)*100+hour)*100+minute)*100+second;
            return nextTimeValue;
        }

        /// <summary>
        /// 入力されたスラッシュ区切り時刻を DateTimeManager に変換(不可ならnull)
        /// </summary>
        /// <param name="strDateTime"></param>
        /// <returns></returns>
        public DateTimeManager ToDateTimeManager(string strDateTime)
        {
            var strDateTimePack = strDateTime.Split('/');
            if (!IsCorrectSintaxOfDateTime(strDateTimePack))
            {
                return null;
            }
            var year   = int.Parse(strDateTimePack[0]);
            var month  = int.Parse(strDateTimePack[1]);
            var day    = int.Parse(strDateTimePack[2]);
            var hour   = int.Parse(strDateTimePack[3]);
            var minute = int.Parse(strDateTimePack[4]);
            var second = int.Parse(strDateTimePack[5]);
            var timeManager = new DateTimeManager(year, month, day, hour, minute, second);
            return timeManager;
        }

        private void Initialize()
        {
            InnerFirstTime     = "1970/1/1/0/00/00";
            InnerNextTime      = "1970/1/1/0/00/00";
            InnerAlarmInterbal = NO_INTERBAL;
            InnerAlarmTimes    = "1";
            InnerLastTime      = "1970/1/1/0/00/00";
            InnerAlarmWay      = "Baloon";
            InnerValidation    = INNER_INVAILID;
        }

        private string ConvertToViewTime(string innerTime)
        {
            var visibleTime = "";
            var strDateTimePack = innerTime.Split('/');
            if (!IsCorrectSintaxOfDateTime(strDateTimePack))
            {
                return null;
            }
            var year   = int.Parse(strDateTimePack[0]);
            var month  = int.Parse(strDateTimePack[1]);
            var day    = int.Parse(strDateTimePack[2]);
            var hour   = int.Parse(strDateTimePack[3]);
            var minute = int.Parse(strDateTimePack[4]);
            var second = int.Parse(strDateTimePack[5]);
            var dt = new DateTime(year, month, day, hour, minute, second);
            visibleTime = dt.ToString("yyyy年MM月dd日(ddd)  HH:mm:ss");
            return visibleTime;
        }

        private string ConvertToViewValidation(string innerValidation)
        {
            if (innerValidation == INNER_VAILID)
            {
                return VALID_TEXT;
            }
            return INVALID_TEXT;
        }

        private bool IsCorrectSintaxOfDateTime(string[] strDateTimePack)
        {
            if (strDateTimePack.Length != NUM_DATE_TIME_ELEMENTS)
            {
                return false;
            }
            if (!IsIntTryParse(strDateTimePack))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 配列の要素全てがint.Parseできるか判定
        /// </summary>
        /// <param name="strAry"></param>
        /// <returns>bool</returns>
        private bool IsIntTryParse(string[] strAry)
        {
            int noused;
            foreach (var str in strAry)
            {
                if (!int.TryParse(str, out noused))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
