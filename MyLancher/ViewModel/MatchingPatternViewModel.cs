﻿using GalaSoft.MvvmLight;
using System.Collections.ObjectModel; /* ObservableCollection */

namespace MyLancher.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MatchingPatternViewModel : ViewModelBase
    {
        private string matchingPattern;

        public string MatchingPattern
        {
            get
            {
                return matchingPattern;
            }
            set
            {
                matchingPattern = value;
                RaisePropertyChanged("MatchingPattern");
            }
        }

        /// <summary>
        /// Initializes a new instance of the MatchingPatternViewModel class.
        /// </summary>
        public MatchingPatternViewModel()
        {
        }

        public void Set(string pattern)
        {
            MatchingPattern = pattern;
        }
    }
}