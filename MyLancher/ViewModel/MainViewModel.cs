/* MainViewModel.cs */
using GalaSoft.MvvmLight; /* ViewModelBase */
using GalaSoft.MvvmLight.Command; /* RelayCommand */
using System.Collections.Generic;
using System.Collections.ObjectModel; /* ObservableCollection */
using System.Windows; /* WindowState */
using MyLancher.Model; /* CatalogManager, ItemDataList */
using System.Windows.Media; /* FontFamily */
using GalaSoft.MvvmLight.Messaging; /* Messenger */
using System.Drawing; /* Icon */

namespace MyLancher.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private const int MAIN_HEIGHT = 80;
        private const int DEFAULT_MENU_CHECK_BAR_HEIGHT = 20;
        private const string FONT_FAMILY  = "DejaVu Sans";
        private const string HELP_TITLE   = "使い方";
        private const string HELP_MESSAGE = "テキストボックスに文字を打つことで、カタログからファイルへのショートカットを呼び出します。\r\n終了するときには[Alt]キーと[F4]キーを同時に押してください。";
        private const string MR_WORLD_ICO = "MyLancher.Pics.world.ico";

        // IO Data //
        private string inputText;
        private string targetName;
        private ObservableCollection<SelectableItemViewModel> visibleItemList;
        private SearchedItem topItem;
        // Manager //
        private LauncherModelManager launcherManager;
        // Layout //
        private int windowHeight;
        private int topIconSize;
        private int topFrameSize;
        private int itemListHeight;
        private System.Windows.Media.FontFamily commonFontFamily;
        private Icon windowIcon;
        // Status //
        private bool defaultMenuVisibility;
        private bool checkBarVisibility;
        // Command //
        private RelayCommand appUpdate;
        private RelayCommand help;
        private RelayCommand setting;
        private RelayCommand pushEnterInTextBox;
        private RelayCommand pushEscape;

        #region Properties
        // IO Data //
        public string InputText
        {
            get
            {
                return inputText;
            }
            set
            {
                if (inputText != value)
                {
                    inputText = value;
                    // HACK: 候補リストの絞込み(そこそこ重たい)
                    LauncherManager.Search(inputText);
                    SetItemList();
                    SetTopItem();
                }
            }
        }

        public string TargetName
        {
            get
            {
                return targetName;
            }
            set
            {
                if (targetName != value)
                {
                    targetName = value;
                    RaisePropertyChanged("TargetName");
                }
            }
        }

        public ObservableCollection<SelectableItemViewModel> VisibleItemList
        {
            get
            {
                return visibleItemList;
            }
            set
            {
                if (visibleItemList != value)
                {
                    visibleItemList = value;
                }
            }
        }

        public SearchedItem TopItem
        {
            get
            {
                return topItem;
            }
            set
            {
                if (topItem != value)
                {
                    topItem = value;
                    RaisePropertyChanged("TopItem");
                }
            }
        }

        // Manager //
        public LauncherModelManager LauncherManager
        {
            get
            {
                return launcherManager;
            }
            set
            {
                if (launcherManager != value)
                {
                    launcherManager = value;
                }
            }
        }

        // Layout //
        public int WindowHeight
        {
            get
            {
                return windowHeight;
            }
            set
            {
                if (windowHeight != value)
                {
                    windowHeight = value;
                    RaisePropertyChanged("WindowHeight");
                }
            }
        }

        public int TopFrameSize
        {
            get
            {
                return topFrameSize;
            }
            set
            {
                if (topFrameSize != value)
                {
                    topFrameSize = value;
                    RaisePropertyChanged("TopFrameSize");
                }
            }
        }

        public int TopIconSize
        {
            get
            {
                return topIconSize;
            }
            set
            {
                if (topIconSize != value)
                {
                    topIconSize = value;
                    RaisePropertyChanged("TopIconSize");
                }
            }
        }

        public int ItemListHeight
        {
            get
            {
                return itemListHeight;
            }
            set
            {
                if (itemListHeight != value)
                {
                    itemListHeight = value;
                    RaisePropertyChanged("ItemListHeight");
                }
            }
        }

        public System.Windows.Media.FontFamily CommonFontFamily
        {
            get
            {
                return commonFontFamily;
            }
            set
            {
                if (commonFontFamily != value)
                {
                    commonFontFamily = value;
                    RaisePropertyChanged("CommonFontFamily");
                }
            }
        }

        public Icon WindowIcon
        {
            get
            {
                return windowIcon;
            }
            set
            {
                if (windowIcon != value)
                {
                    windowIcon = value;
                    RaisePropertyChanged("WindowIcon");
                }
            }
        }

        // Status //
        public bool DefaultMenuVisibility
        {
            get
            {
                return defaultMenuVisibility;
            }
            set
            {
                if (defaultMenuVisibility != value)
                {
                    defaultMenuVisibility = value;
                    LauncherManager.DefaultMenuVisibleState = value;
                    SetDefautMenuList();
                    RaisePropertyChanged("DefaultMenuVisibility");
                }
            }
        }

        public bool CheckBarVisibility
        {
            get
            {
                return checkBarVisibility;
            }
            set
            {
                if (checkBarVisibility != value)
                {
                    checkBarVisibility = value;
                    RaisePropertyChanged("CheckBarVisibility");
                }
            }
        }

        // Command //
        public RelayCommand AppUpdate
        {
            get
            {
                if (appUpdate == null)
                {
                    appUpdate = new RelayCommand(() =>
                    {
                        // HACK: Helpの改善
                        ShowHelp();
                    });
                }
                return appUpdate;
            }
        }

        public RelayCommand Help
        {
            get
            {
                if (help == null)
                {
                    help = new RelayCommand(() =>
                    {
                        // HACK: Helpの改善
                        ShowHelp();
                    });
                }
                return help;
            }
        }

        public RelayCommand Setting
        {
            get
            {
                if (setting == null)
                {
                    setting = new RelayCommand(() =>
                    {
                        SettingWindow setWindow = new SettingWindow();
                        setWindow.Show();
                    });
                }
                return setting;
            }
        }

        public RelayCommand PushEnter
        {
            get
            {
                if (pushEnterInTextBox == null)
                {
                    pushEnterInTextBox = new RelayCommand(() =>
                    {
                        if (TopItem.ItemName == "")
                        {
                            InvisibleWindow();
                            return;
                        }
                        TopItem.Execute();
                    });
                }
                return pushEnterInTextBox;
            }
        }

        public RelayCommand PushEscape
        {
            get
            {
                if (pushEscape == null)
                {
                    pushEscape = new RelayCommand(() =>
                    {
                        InvisibleWindow();
                    });
                }
                return pushEscape;
            }
        }
        #endregion

        public MainViewModel()
        {
            Messenger.Default.Register<NotificationMessage>(this, "update", msg =>
            {
                UpdateWindow();
            });

            ////if (IsInDesignMode)
            ////{
            ////    // Code runs in Blend --> create design time data.
            ////}
            ////else
            ////{
            ////    // Code runs "for real"
            ////}

            Initialize();
        }

        private void Initialize()
        {
            LogFileManager logManager = new LogFileManager();
            try
            {
                var comPic = new CommonsPict();
                WindowIcon = comPic.GetWorldIcon();

                LauncherManager = new LauncherModelManager();
                VisibleItemList = new ObservableCollection<SelectableItemViewModel>();
                TopItem = new SearchedItem("", "", comPic.ToBitmapImage(comPic.GetWorldIcon()));
                InitializeWindowLayout();
                SetSettingStatus();
            }
            catch (System.Exception ex)
            {
                logManager.Add(ex.Message);
                logManager.Execute();
            }
        }

        private void InitializeWindowLayout()
        {
            TopIconSize  = 32;/* 16,32,48 */
            TopFrameSize = TopIconSize + 10;
            WindowHeight = MAIN_HEIGHT;
            InputText    = "";
            TargetName   = "";
            CommonFontFamily = new System.Windows.Media.FontFamily(FONT_FAMILY);
        }

        /// <summary>
        /// Windowを不可視にするためのメッセージを飛ばします。
        /// </summary>
        private void InvisibleWindow()
        {
            var message = new NotificationMessage(this, "message");
            Messenger.Default.Send(message, "invisibleLauncher");
        }

        private void SetDefautMenuList()
        {
            LauncherManager.SetDefautMenuList();
            SetItemList();
        }

        private void SetItemList()
        {
            VisibleItemList.Clear();
            LauncherManager.SetVisibleItemList();
            VisibleItemList = LauncherManager.VisibleItemList;
            SetWindowLayout();
        }

        private void SetSettingStatus()
        {
            var settingData = new GeneralSettingFileManager().GetSettingData();
            DefaultMenuVisibility = settingData.DefautMenuVisibility;
            CheckBarVisibility = settingData.CheckBarVisibility;
            LauncherManager.SetSettingStatus(settingData);
        }

        private void SetTopItem()
        {
            try
            {
                TopItem = LauncherManager.GetMatchItemIndexOf(0);
            }
            catch(System.ArgumentException)
            {
                var comPic = new CommonsPict();
                TopItem = new SearchedItem("", "", comPic.ToBitmapImage(comPic.GetWorldIcon()));
            }
        }

        private void SetWindowLayout()
        {
            ItemListHeight = TopIconSize * (LauncherManager.VisibleItemCount);
            WindowHeight = MAIN_HEIGHT + ItemListHeight + DEFAULT_MENU_CHECK_BAR_HEIGHT;
        }

        /// <summary>
        /// ヘルプを呼び出します。
        /// </summary>
        private void ShowHelp()
        {
            MessageBox.Show(HELP_MESSAGE, HELP_TITLE);
        }

        /// <summary>
        /// Windowを更新します。
        /// </summary>
        private void UpdateWindow()
        {
            SetSettingStatus();
            LauncherManager.ResetIndexManager();
        }
    }
}