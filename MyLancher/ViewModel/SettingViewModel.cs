﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System.IO;
using MyLancher.Model; /* Constants.cs */
using System.Collections.ObjectModel; /* ObservableCollection */

namespace MyLancher.ViewModel
{
    /// <summary>
    /// 設定ViewModel
    /// </summary>
    public class SettingViewModel : ViewModelBase
    {
        /* Manager */
        private SettingModelManager settingManager;
        /* Launcher Setting */
        //private LauncherSettingData launcherSettingList;
        private bool defautMenuVisibility;
        private bool checkBarVisibility;
        private bool notIgnoreCase;
        private bool searchState;
        private bool wildAvailability;
        /* Catalog Setting */
        /** Directory List **/
        private DirectoryDataList targetDirectoryList;
        private ObservableCollection<NotifyDirectory> visibleDirectoryList;
        private string selectedDirectoryPath;
        private string additionalDirectoryPath;
        private RelayCommand addDirectoryPath;
        private RelayCommand deleteDirectoryPath;
        /** Matching Pattern **/
        private ObservableCollection<NotifyMatchingPattern> visibleMatchingPattern;
        private string selectedMatchingPattern;
        private string additionalMatchingPattern;
        private RelayCommand addMatchingPattern;
        private RelayCommand deleteMatchingPattern;
        /** Index Field **/
        private string numberOfIndex;
        private RelayCommand resetIndex;
        /* Command of All Setting */
        private RelayCommand applyToClose;
        private RelayCommand refreshToClose;

        public SettingModelManager SettingManager
        {
            get
            {
                return settingManager;
            }
            set
            {
                if (settingManager != value)
                {
                    settingManager = value;
                }
            }
        }

        //public LauncherSettingData LauncherSettingList
        //{
        //    get
        //    {
        //        return launcherSettingList;
        //    }
        //    set
        //    {
        //        if (launcherSettingList != value)
        //        {
        //            launcherSettingList = value;
        //        }
        //    }
        //}

        public bool DefautMenuVisibility
        {
            get
            {
                return defautMenuVisibility;
            }
            set
            {
                defautMenuVisibility = value;
                SettingManager.DefautMenuVisibility = value;
                RaisePropertyChanged("DefautMenuVisibility");
            }
        }

        public bool CheckBarVisibility
        {
            get
            {
                return checkBarVisibility;
            }
            set
            {
                checkBarVisibility = value;
                SettingManager.CheckBarVisibility = value;
                RaisePropertyChanged("CheckBarVisibility");
            }
        }

        public bool NotIgnoreCase
        {
            get
            {
                return notIgnoreCase;
            }
            set
            {
                notIgnoreCase = value;
                SettingManager.NotIgnoreCase = value;
                RaisePropertyChanged("IgnoreCase");
            }
        }

        public bool SearchState
        {
            get
            {
                return searchState;
            }
            set
            {
                searchState = value;
                SettingManager.SearchIncludeSubdirectory = value;
                RaisePropertyChanged("SearchState");
            }
        }

        public bool WildAvailability
        {
            get
            {
                return wildAvailability;
            }
            set
            {
                wildAvailability = value;
                SettingManager.WildAvailability = value;
                RaisePropertyChanged("WildAvailability");
            }
        }

        public ObservableCollection<NotifyDirectory> VisibleDirectoryList
        {
            get
            {
                return visibleDirectoryList;
            }
            set
            {
                if (visibleDirectoryList != value)
                {
                    visibleDirectoryList = value;
                }
            }
        }

        public DirectoryDataList TargetDirectoryList
        {
            get
            {
                return targetDirectoryList;
            }
            set
            {
                if (targetDirectoryList != value)
                {
                    targetDirectoryList = value;
                }
            }
        }

        public ObservableCollection<NotifyMatchingPattern> VisibleMatchingPattern
        {
            get
            {
                return visibleMatchingPattern;
            }
            set
            {
                if (visibleMatchingPattern != value)
                {
                    visibleMatchingPattern = value;
                }
            }
        }

        public string SelectedDirectoryPath
        {
            get
            {
                return selectedDirectoryPath;
            }
            set
            {
                selectedDirectoryPath = value;
                // 検索パターンを設定
                SetVisibleMatchingPattern(SelectedDirectoryPath);
                RaisePropertyChanged("SelectedDirectoryPath");
            }
        }

        public string AdditionalDirectoryPath
        {
            get
            {
                return additionalDirectoryPath;
            }
            set
            {
                additionalDirectoryPath = value;
            }
        }

        public RelayCommand AddDirectoryPath
        {
            get
            {
                if (addDirectoryPath == null)
                {
                    addDirectoryPath = new RelayCommand(() =>
                    {
                        if (AdditionalDirectoryPath == "")
                        {
                            return;
                        }
                        if (!Directory.Exists(AdditionalDirectoryPath))
                        {
                            System.Windows.Forms.MessageBox.Show("このディレクトリは存在しません。");
                            return;
                        }
                        // TODO: 既出をはじく
                        var nDirectory = new NotifyDirectory(AdditionalDirectoryPath);
                        VisibleDirectoryList.Add(nDirectory);
                        var directoryData = new DirectoryData();
                        directoryData.DirectoryPath = AdditionalDirectoryPath;
                        TargetDirectoryList.Add(directoryData);
                        System.Console.Write("Add Directory");
                    });
                }
                return addDirectoryPath;
            }
        }

        public RelayCommand DeleteDirectoryPath
        {
            get
            {
                if (deleteDirectoryPath == null)
                {
                    deleteDirectoryPath = new RelayCommand(() =>
                    {
                        var nDirectory = GetPathToBeDeleted();
                        VisibleDirectoryList.Remove(nDirectory);
                        TargetDirectoryList.RemoveDerectoryPath(nDirectory.Path);
                    });
                }
                return deleteDirectoryPath;
            }
        }

        public string SelectedMatchingPattern
        {
            get
            {
                return selectedMatchingPattern;
            }
            set
            {
                selectedMatchingPattern = value;
                RaisePropertyChanged("SelectedMatchingPattern");
            }
        }

        public string AdditionalMatchingPattern
        {
            get
            {
                return additionalMatchingPattern;
            }
            set
            {
                additionalMatchingPattern = value;
            }
        }

        public RelayCommand AddMatchingPattern
        {
            get
            {
                if (addMatchingPattern == null)
                {
                    addMatchingPattern = new RelayCommand(() =>
                    {
                        var nPattern = new NotifyMatchingPattern(AdditionalMatchingPattern);
                        VisibleMatchingPattern.Add(nPattern);
                        TargetDirectoryList.AddPattern(nPattern.Pattern, SelectedDirectoryPath);
                    });
                }
                return addMatchingPattern;
            }
        }

        public RelayCommand DeleteMatchingPattern
        {
            get
            {
                if (deleteMatchingPattern == null)
                {
                    deleteMatchingPattern = new RelayCommand(() =>
                    {
                        var nPattern = GetPatternToBeDeleted();
                        VisibleMatchingPattern.Remove(nPattern);
                        TargetDirectoryList.RemovePattern(nPattern.Pattern, SelectedDirectoryPath);
                    });
                }
                return deleteMatchingPattern;
            }
        }

        public RelayCommand ApplyToClose
        {
            get
            {
                if (applyToClose == null)
                {
                    applyToClose = new RelayCommand(() =>
                    {
                        var message = new GalaSoft.MvvmLight.Messaging.NotificationMessage(this, "message");
                        GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(message, "close");
                        SettingManager.SetSettingData();
                        SettingManager.WriteDirectoryList(TargetDirectoryList);
                        GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(message, "update");
                    });
                }
                return applyToClose;
            }
        }

        public RelayCommand RefreshToClose
        {
            get
            {
                if (refreshToClose == null)
                {
                    refreshToClose = new RelayCommand(() =>
                    {
                        var message = new GalaSoft.MvvmLight.Messaging.NotificationMessage(this, "message");
                        GalaSoft.MvvmLight.Messaging.Messenger.Default.Send(message, "close");
                        Initialize();
                    });
                }
                return refreshToClose;
            }
        }

        public RelayCommand ResetIndex
        {
            get
            {
                if (resetIndex == null)
                {
                    resetIndex = new RelayCommand(() =>
                    {
                        CreateIndexFile();
                        SetIndex();
                    });
                }
                return resetIndex;
            }
        }

        public string NumberOfIndex
        {
            get
            {
                return numberOfIndex;
            }
            set
            {
                if (numberOfIndex != value)
                {
                    numberOfIndex = value;
                    RaisePropertyChanged("NumberOfIndex");
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the SettingViewModel class.
        /// </summary>
        public SettingViewModel()
        {
            Initialize();
        }

        private void Initialize()
        {
            SettingManager = new SettingModelManager();
            InitializeLauncherSetting();
            InitializeCatalogSetting();
        }

        /// <summary>
        /// ランチャーの基本設定の初期化。
        /// </summary>
        private void InitializeLauncherSetting()
        {
            DefautMenuVisibility = SettingManager.DefautMenuVisibility;
            CheckBarVisibility   = SettingManager.CheckBarVisibility;
            NotIgnoreCase        = SettingManager.NotIgnoreCase;
            SearchState          = SettingManager.SearchIncludeSubdirectory;
            WildAvailability     = SettingManager.WildAvailability;
        }

        /// <summary>
        /// カタログ設定の初期化
        /// </summary>
        private void InitializeCatalogSetting()
        {
            var t1 = System.DateTime.Now;
            TargetDirectoryList = SettingManager.GetDirectoryList();
            var t2 = System.DateTime.Now;
            var d1 = t2 - t1;
            VisibleDirectoryList = new ObservableCollection<NotifyDirectory>();
            var t3 = System.DateTime.Now;
            var d2 = t3 - t2;
            VisibleMatchingPattern = new ObservableCollection<NotifyMatchingPattern>();
            var t4 = System.DateTime.Now;
            var d3 = t4 - t3;
            InitializeVisibleDirectoryList();
            var t5 = System.DateTime.Now;
            var d4 = t5 - t4;
            SetIndex();
            var t6 = System.DateTime.Now;
            var d5 = t6 - t5;
            System.Console.WriteLine(d1.ToString());
            System.Console.WriteLine(d2.ToString());
            System.Console.WriteLine(d3.ToString());
            System.Console.WriteLine(d4.ToString());
            System.Console.WriteLine(d5.ToString());
        }

        /// <summary>
        /// ディレクトリリスト表示の初期化。
        /// </summary>
        private void InitializeVisibleDirectoryList()
        {
            VisibleDirectoryList.Clear();
            var targetDirectoryCollection = TargetDirectoryList.GetCollectionList();
            if (targetDirectoryCollection.Count <= 0)
            {
                return;
            }
            foreach (var data in targetDirectoryCollection)
            {
                var path = new NotifyDirectory(data.DirectoryPath);
                VisibleDirectoryList.Add(path);
            }
            SelectedDirectoryPath = targetDirectoryCollection[0].DirectoryPath;
        }

        public void SetVisibleMatchingPattern(string directoryPath)
        {
            VisibleMatchingPattern.Clear();
            var matchingPatternCollection = TargetDirectoryList.GetMatchingPattern(directoryPath);
            if (matchingPatternCollection == null)
            {
                return;
            }
            foreach (var pattern in matchingPatternCollection)
            {
                var matchingPattern = new NotifyMatchingPattern(pattern);
                VisibleMatchingPattern.Add(matchingPattern);
            }
        }

        private void CreateIndexFile()
        {
            SettingManager.WriteDirectoryList(TargetDirectoryList);
            var indexCatalog = new IndexCatalog();
            indexCatalog.RemakeIndexFile();
        }

        private void SetIndex()
        {
            var indexCount = new IndexFileManager().GetNumberOfItems();
            NumberOfIndex = "アイテム数: " + indexCount.ToString() + "";
        }

        private NotifyDirectory GetPathToBeDeleted()
        {
            var targetDirectory = new NotifyDirectory(SelectedDirectoryPath);
            foreach (var nDirectory in VisibleDirectoryList)
            {
                if (nDirectory.Path == SelectedDirectoryPath)
                {
                    targetDirectory = nDirectory;
                    return targetDirectory;
                }
            }
            return targetDirectory;
        }

        private NotifyMatchingPattern GetPatternToBeDeleted()
        {
            var targetPattern = new NotifyMatchingPattern(SelectedMatchingPattern);
            foreach (var nPattern in VisibleMatchingPattern)
            {
                if (nPattern.Pattern == SelectedMatchingPattern)
                {
                    targetPattern = nPattern;
                    return targetPattern;
                }
            }
            return targetPattern;
        }
    }
}