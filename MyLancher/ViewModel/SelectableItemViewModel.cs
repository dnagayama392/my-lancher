﻿using GalaSoft.MvvmLight;
using MyLancher.Model;
using System.Windows.Media.Imaging; /* BitmapImage */
using GalaSoft.MvvmLight.Command; /* RelayCommand */
using System.Diagnostics; /* Process */

namespace MyLancher.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class SelectableItemViewModel : ViewModelBase
    {
        private string itemName;
        private string itemPath;
        private BitmapImage iconImage;
        private int iconWidth;
        private RelayCommand executeItem;

        public string ItemName
        {
            get
            {
                return itemName;
            }
            set
            {
                if (itemName != value)
                {
                    itemName = value;
                    RaisePropertyChanged("ItemName");
                }
            }
        }

        public string ItemPath
        {
            get
            {
                return itemPath;
            }
            set
            {
                if (itemPath != value)
                {
                    itemPath = value;
                    //RaisePropertyChanged("ItemPath");
                }
            }
        }

        public BitmapImage IconImage
        {
            get
            {
                return iconImage;
            }
            set
            {
                if (iconImage != value)
                {
                    iconImage = value;
                    RaisePropertyChanged("IconImage");
                }
            }
        }

        public int IconWidth
        {
            get
            {
                return iconWidth;
            }
            set
            {
                if (iconWidth != value)
                {
                    iconWidth = value;
                    RaisePropertyChanged("IconWidth");
                }
            }
        }

        public RelayCommand ExecuteItem
        {
            get
            {
                if (executeItem == null)
                {
                    executeItem = new RelayCommand(() =>
                    {
                        ExecuteItemMethod();
                    });
                }
                return executeItem;
            }
        }

        /// <summary>
        /// Initializes a new instance of the SelectItemViewModel class.
        /// </summary>
        public SelectableItemViewModel(SearchedItem itemData)
        {
            ItemName  = itemData.ItemName;
            ItemPath  = itemData.ItemPath;
            IconImage = itemData.IconBitmapImage;
            IconWidth = 32;
        }

        private void ExecuteItemMethod()
        {
            if (ItemPath == null)
            {
                System.Windows.Forms.MessageBox.Show("この機能は未実装です。");
                return;
            }
            if (ItemPath == "ToDo")
            {
                var todoWindow = new ToDoWindow();
                todoWindow.Show();
                return;
            }
            if (ItemPath == "Alarm")
            {
                var todoWindow = new AlarmWindow();
                todoWindow.Show();
                return;
            }
            try
            {
                Process.Start(ItemPath);
            }
            catch (System.IO.FileNotFoundException)
            {
                System.Windows.Forms.MessageBox.Show("対象のファイルは存在しません。カタログを再設定してください。");
                return;
            }
            catch (System.ObjectDisposedException)
            {
                return;
            }
            catch (System.ComponentModel.Win32Exception)
            {
                return;
            }
        }
    }
}