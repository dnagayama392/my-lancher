﻿/* ToDoViewModel.cs */
using GalaSoft.MvvmLight; /* RaisePropertyChanged */
using GalaSoft.MvvmLight.Command; /* RelayCommand */
using System.Collections.ObjectModel; /* ObservableCollection */
using MyLancher.Model; /* ToDoData */
using GalaSoft.MvvmLight.Messaging; /* Messenger */
using System.Windows.Media.Imaging; /* BitmapImage */

namespace MyLancher.ViewModel
{
    public class ToDoViewModel : ViewModelBase
    {
        private int toDoWindowWidth;
        private int toDoListBarWidth;
        private int toDoListViewerHeight;
        private BitmapImage todoIcon;
        private ObservableCollection<NotifyToDoData> toDoList;
        private ToDoFileManager toDoManager;
        private string topText;
        private NotifyToDoData selectedToDo;
        private RelayCommand addToDo;
        private RelayCommand deleteToDo;
        private RelayCommand closeApply;
        private RelayCommand closeRefresh;
        private RelayCommand apply;

        public int ToDoWindowWidth
        {
            get
            {
                return toDoWindowWidth;
            }
            set
            {
                if (toDoWindowWidth != value)
                {
                    toDoWindowWidth = value;
                    RaisePropertyChanged("ToDoWindowWidth");
                }
            }
        }

        public int ToDoListBarWidth
        {
            get
            {
                return toDoListBarWidth;
            }
            set
            {
                if (toDoListBarWidth != value)
                {
                    toDoListBarWidth = value;
                    RaisePropertyChanged("ToDoListBarWidth");
                }
            }
        }

        public int ToDoListViewerHeight
        {
            get
            {
                return toDoListViewerHeight;
            }
            set
            {
                if (toDoListViewerHeight != value)
                {
                    toDoListViewerHeight = value;
                    RaisePropertyChanged("ToDoListViewerHeight");
                }
            }
        }

        public BitmapImage ToDoIcon
        {
            get
            {
                return todoIcon;
            }
            set
            {
                if (todoIcon != value)
                {
                    todoIcon = value;
                    RaisePropertyChanged("ToDoIcon");
                }
            }
        }

        public ObservableCollection<NotifyToDoData> ToDoList
        {
            get
            {
                return toDoList;
            }
            set
            {
                if (toDoList != value)
                {
                    toDoList = value;
                }
            }
        }

        public ToDoFileManager ToDoManager
        {
            get
            {
                return toDoManager;
            }
            set
            {
                if (toDoManager != value)
                {
                    toDoManager = value;
                }
            }
        }

        public string TopText
        {
            get
            {
                return topText;
            }
            set
            {
                if (topText != value)
                {
                    topText = value;
                    RaisePropertyChanged("TopText");
                }
            }
        }

        public NotifyToDoData SelectedToDo
        {
            get
            {
                return selectedToDo;
            }
            set
            {
                if (selectedToDo != value)
                {
                    selectedToDo = value;
                    RaisePropertyChanged("SelectedToDo");
                }
            }
        }

        public RelayCommand AddToDo
        {
            get
            {
                if (addToDo == null)
                {
                    addToDo = new RelayCommand(() =>
                    {
                        AddNewToDo();
                    });
                }
                return addToDo;
            }
        }

        public RelayCommand DeleteToDo
        {
            get
            {
                if (deleteToDo == null)
                {
                    deleteToDo = new RelayCommand(() =>
                    {
                        DeleteSelectedToDo();
                    });
                }
                return deleteToDo;
            }
        }

        public RelayCommand CloseApply
        {
            get
            {
                if (closeApply == null)
                {
                    closeApply = new RelayCommand(() =>
                    {
                        CloseToDoWindow();
                        ApplyToFile();
                    });
                }
                return closeApply;
            }
        }

        public RelayCommand CloseRefresh
        {
            get
            {
                if (closeRefresh == null)
                {
                    closeRefresh = new RelayCommand(() =>
                    {
                        CloseToDoWindow();
                        Refresh();
                    });
                }
                return closeRefresh;
            }
        }

        public RelayCommand Apply
        {
            get
            {
                if (apply == null)
                {
                    apply = new RelayCommand(() =>
                    {
                        ApplyToFile();
                    });
                }
                return apply;
            }
        }

        public ToDoViewModel()
        {
            Initialize();
        }

        private void Initialize()
        {
            var comPic = new CommonsPict();
            ToDoIcon = comPic.ToBitmapImage(comPic.GetListIcon());
            ToDoList = new ObservableCollection<NotifyToDoData>();
            ToDoManager = new ToDoFileManager();
            SetToDoList();
            SetWindowLayout();
        }

        private void AddNewToDo()
        {
            if (TopText == "")
            {
                return;
            }
            ToDoList.Insert(0, new NotifyToDoData(TopText));
            TopText = "";
            SelectedToDo = ToDoList[0];
        }

        private void ApplyToFile()
        {
            ToDoManager.Write(ToDoList);
        }

        private void CloseToDoWindow()
        {
            var message = new NotificationMessage(this, "message");
            Messenger.Default.Send(message, "closeToDo");
        }

        private void DeleteSelectedToDo()
        {
            if (SelectedToDo == null)
            {
                return;
            }
            var index = ToDoList.IndexOf(SelectedToDo);
            ToDoList.Remove(SelectedToDo);
            if (ToDoList.Count == 0)
            {
                SelectedToDo = null;
                return;
            }
            if (index == ToDoList.Count)
            {
                index = ToDoList.Count - 1;
            }
            SelectedToDo = ToDoList[index];
        }

        private void Refresh()
        {
            ToDoList.Clear();
            SetToDoList();
        }

        private void SetToDoList()
        {
            foreach(var toDoData in ToDoManager.Read())
            {
                ToDoList.Add(toDoData);
            }
            SelectedToDo = ToDoList[0];
        }

        private void SetWindowLayout()
        {
            ToDoWindowWidth = 500;
            ToDoListBarWidth = 190;
            ToDoListViewerHeight = 220;
        }
    }
}